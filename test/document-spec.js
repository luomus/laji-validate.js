import {lajiValidate, expect} from "./bootstrap";

describe("document", function() {
  it("supports complex validations", function() {
    const schema = {
      additionalId: {
        items: {
          length: {minimum: 3}
        }
      },
      gatherings: {
        items: {
          properties: {
            dateBegin: {
              datetime: {
                earliest: "2015-01-01",
                latest: "now",
                dateOnly: true
              }
            },
            units: {
              items: {
                properties: {
                  individualCount: {
                    numericality: {
                      greaterThan: 0
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
    const validCase = {
      additionalId: ["test"],
      gatherings: [
        {
          dateBegin: "2015-01-01"
        },
        {
          dateBegin: "2015-01-31"
        }
      ]
    };
    const invalidCase = {
      additionalId: ["s"],
      gatherings: [

        {
          dateBegin: "2014-01-31",
          units: [
            {
              individualCount: 0
            }
          ]
        }
      ]
    };
    expect(lajiValidate(validCase, schema)).to.be(undefined);
    expect(lajiValidate(invalidCase, schema)).to.eql({
      "additionalId[0]": [
        "is too short (minimum is 3 characters)"
      ],
      "gatherings[0].dateBegin": [
        "must be no earlier than 2015-01-01"
      ],
      "gatherings[0].units[0].individualCount": [
        "must be greater than 0"
      ]
    });
  });

  it("supports valid async validations", function(done) {
    const schema = {
      additionalId: {
        items: {
          length: {minimum: 3}
        }
      },
      gatherings: {
        items: {
          properties: {
            dateBegin: {
              datetime: {
                earliest: "2015-01-01",
                latest: "now",
                dateOnly: true
              }
            },
            units: {
              items: {
                properties: {
                  individualCount: {
                    numericality: {
                      greaterThan: 0
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
    const validCase = {
      additionalId: ["test"],
      gatherings: [
        {
          dateBegin: "2015-01-01"
        },
        {
          dateBegin: "2015-01-31"
        }
      ]
    };
    function success() {
      done();
    }
    function error(err) {
      done(err);
    }
    lajiValidate.async(validCase, schema)
      .then(success, error);
  });

  it("supports invalid async validations", function(done) {
    const schema = {
      additionalId: {
        items: {
          length: {minimum: 3}
        }
      },
      gatherings: {
        items: {
          properties: {
            dateBegin: {
              datetime: {
                earliest: "2015-01-01",
                latest: "now",
                dateOnly: true
              }
            },
            units: {
              items: {
                properties: {
                  individualCount: {
                    numericality: {
                      greaterThan: 0
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
    const invalidCase = {
      additionalId: ["s"],
      gatherings: [

        {
          dateBegin: "2014-01-31",
          units: [
            {
              individualCount: 0
            }
          ]
        }
      ]
    };
    function success() {
      done("Should have errors but none where found!");
    }
    function error() {
      done();
    }
    lajiValidate.async(invalidCase, schema)
      .then(success, error);
  });

  it("supports skipping fields with skipped property", function(done) {
    const schema = {
      additionalId: {
        items: {
          length: {minimum: 3}
        }
      },
      gatherings: {
        items: {
          properties: {
            dateBegin: {
              datetime: {
                earliest: "2015-01-01",
                latest: "now",
                dateOnly: true
              }
            },
            units: {
              items: {
                properties: {
                  individualCount: {
                    numericality: {
                      greaterThan: 0
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
    const validCase = {
      additionalId: ["abc"],
      gatherings: [
        {
          dateBegin: "2015-01-02",
          units: [
            {
              individualCount: 1
            }
          ]
        },
        {
          skipped: true,
          dateBegin: "2014-01-31",
          units: [
            {
              individualCount: 0
            }
          ]
        }
      ]
    };
    function success() {
      done();
    }
    function error(err) {
      done(err);
    }
    lajiValidate.async(validCase, schema)
      .then(success, error);
  });

  it("does not skip field if skipped property is false", function(done) {
    const schema = {
      additionalId: {
        items: {
          length: {minimum: 3}
        }
      },
      gatherings: {
        items: {
          properties: {
            dateBegin: {
              datetime: {
                earliest: "2015-01-01",
                latest: "now",
                dateOnly: true
              }
            },
            units: {
              items: {
                properties: {
                  individualCount: {
                    numericality: {
                      greaterThan: 0
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
    const invalidCase = {
      additionalId: ["abc"],
      gatherings: [
        {
          dateBegin: "2015-01-01",
          units: [
            {
              individualCount: 1
            }
          ]
        },
        {
          skipped: false,
          dateBegin: "2014-01-31",
          units: [
            {
              individualCount: 0
            }
          ]
        }
      ]
    };
    function success() {
      done("Should have errors but none where found!");
    }
    function error() {
      done();
    }
    lajiValidate.async(invalidCase, schema)
      .then(success, error);
  });

  it("gives correct validations to other fields when one field is skipped", function() {
    const schema = {
      additionalId: {
        items: {
          length: {minimum: 3}
        }
      },
      gatherings: {
        items: {
          properties: {
            dateBegin: {
              datetime: {
                earliest: "2015-01-01",
                latest: "now",
                dateOnly: true
              }
            },
            units: {
              items: {
                properties: {
                  individualCount: {
                    numericality: {
                      greaterThan: 0
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
    const invalidCase = {
      additionalId: ["t"],
      gatherings: [
        {
          dateBegin: "2014-01-31",
          units: [
            {
              individualCount: 1
            }
          ]
        },
        {
          skipped: true,
          dateBegin: "2014-01-31",
          units: [
            {
              individualCount: 0
            }
          ]
        },
        {
          dateBegin: "2015-01-01",
          units: [
            {
              individualCount: 0
            }
          ]
        },
        {
          dateBegin: "2015-01-01",
          units: [
            {
              skipped: true,
              individualCount: 0
            }
          ]
        }
      ]
    };
    expect(lajiValidate(invalidCase, schema)).to.eql({
      "additionalId[0]": [
        "is too short (minimum is 3 characters)"
      ],
      "gatherings[0].dateBegin": [
        "must be no earlier than 2015-01-01"
      ],
      "gatherings[2].units[0].individualCount": [
        "must be greater than 0"
      ]
    });
  });
});
