import {lajiValidate, expect} from "../bootstrap";

describe("validators.crossCheck", function() {

  const {crossCheck} = lajiValidate.validators;

  it("allows empty object", function() {
    expect(crossCheck({}, {check: "bar"}, undefined, {})).to.be(undefined);
    expect(crossCheck({}, {check: "bar", mustBeEmpty: true})).to.be(undefined);
    expect(crossCheck("", {check: "bar"})).to.be(undefined);
  });

  it("allows valid simple data", function() {
    expect(crossCheck("haa", {check: "bar"}, undefined, {})).to.be(undefined);
    expect(crossCheck("haa", {check: "bar"}, undefined, {})).to.be(undefined);
    expect(crossCheck(2, {check: "bar"}, undefined, {"bar": 3})).to.be(undefined);
    expect(crossCheck(0, {check: "bar"}, undefined, {"bar": 7})).to.be(undefined);
    expect(crossCheck("", {check: "bar", mustBeEmpty: true}, undefined, {})).to.be(undefined);
    expect(crossCheck("hoo", {check: "bar", mustBeEmpty: true}, undefined, {})).to.be(undefined);
  });

  it("doesn't allow value when value is incorrect", function() {
    const msg = "msg";
    expect(crossCheck("", {check: "bar", message: msg}, "1", {foo: "", bar: "hoo"})).to.be(msg);
    expect(crossCheck("haa", {check: "bar", message: msg, mustBeEmpty: true}, "2", {foo: "haa", bar: "hoo"})).to.be(msg);
    expect(crossCheck(undefined, {check: "bar", message: msg}, "3", {bar: "hoo"})).to.be(msg);
  });

  it("checks that onlyWhenCheckIn option is working", function() {
    const msg = "msg";
    expect(crossCheck("", {check: "bar", onlyWhenCheckIn: ["o"], message: msg}, "1", {foo: "", bar: "hoo"})).to.be(undefined);
    expect(crossCheck("", {check: "bar", onlyWhenCheckIn: ["hoo"], message: msg}, "2", {foo: "", bar: "hoo"})).to.be(msg);
    expect(crossCheck("", {check: "bar", onlyWhenCheckIn: ["hoo"], mustBeEmpty: true, message: msg}, "3", {foo: "", bar: "hoo"})).to.be(undefined);
  });

  it("checks that onlyWhenCheckNotIn option is working", function() {
    const msg = "msg";
    expect(crossCheck("", {check: "bar", onlyWhenCheckNotIn: ["o"], message: msg}, "1", {foo: "", bar: "hoo"})).to.be(msg);
    expect(crossCheck("", {check: "bar", onlyWhenCheckNotIn: ["hoo"], message: msg}, "2", {foo: "", bar: "hoo"})).to.be(undefined);
    expect(crossCheck("", {check: "bar", onlyWhenCheckNotIn: ["o"], mustBeEmpty: true, message: msg}, "3", {foo: "", bar: "hoo"})).to.be(undefined);
  });

  it("allow json pointer as check value", function() {
    const msg = "msg";
    expect(crossCheck("", {check: "/bar/t1", message: msg}, "1", {foo: "", bar: {t1: "hoo"}})).to.be(msg);
    expect(crossCheck("jee", {check: "/bar/t1", message: msg}, "2", {foo: "jee", bar: {t1: "hoo"}})).to.be(undefined);

    expect(crossCheck("", {check: "/bar/0/t1", message: msg}, "3", {foo: "", bar: [{t1: "hoo"}]})).to.be(msg);
    expect(crossCheck("jee", {check: "/bar/0/t1", message: msg}, "4", {foo: "jee", bar: [{t1: "hoo"}]})).to.be(undefined);

    expect(crossCheck("jee", {check: "/bar/1/t1", requireCheck: true, message: msg}, "5", {foo: "jee", bar: [{t1: "hoo"}]})).to.be(msg);

  });

  it("allows json pointer to array as check value", function() {
    const msg = "msg";
    expect(crossCheck("", {check: "/bar/*/t1", message: msg}, "1", {foo: "", bar: [{t1: undefined}, {t1: "hoo"}]})).to.be(msg);
    expect(crossCheck("jee", {check: "/bar/*/t1", message: msg}, "2", {foo: "jee", bar: [{t1: "hoo"}, {t1: "haa"}]})).to.be(undefined);
    expect(crossCheck("", {check: "/bar/*/t1", message: msg}, "3", {foo: "jee", bar: [{t1: undefined, t2: "hoo"}, {t2: "haa"}]})).to.be(undefined);
  });

  it("allows array of check values", function() {
    const msg = "msg";
    expect(crossCheck("", {check: ["bar", "foo"], message: msg}, "1", {foo: "abc", bar: undefined})).to.be(msg);
    expect(crossCheck("jee", {check: ["bar", "foo"], message: msg}, "2", {foo: "jee", bar: "jee"})).to.be(undefined);
    expect(crossCheck("", {check: ["bar", "foo"], message: msg}, "3", {foo: undefined, bar: undefined})).to.be(undefined);
  });

  it("checks that valueMustBeIn option is working", function() {
    const msg = "msg";
    expect(crossCheck("o", {check: "bar", valueMustBeIn: ["o"], message: msg}, "1", {foo: "", bar: "hoo"})).to.be(undefined);
    expect(crossCheck("a", {check: "bar", valueMustBeIn: ["c"], message: msg}, "2", {foo: "", bar: "hoo"})).to.be(msg);
    expect(crossCheck("a", {check: "bar", onlyWhenCheckIn: ["hoo"], valueMustBeIn: ["a", "b"], message: msg}, "3", {foo: "", bar: "hoo"})).to.be(undefined);
    expect(crossCheck("", {check: "bar", mustHaveValue: false, valueMustBeIn: ["o"], message: msg}, "1", {foo: "", bar: "hoo"})).to.be(undefined);
  });

  it("checks that onlyWhenNoCheck option is working", function() {
    const msg = "msg";
    expect(crossCheck("", {check: "bar", onlyWhenNoCheck: true, message: msg}, "1", {foo: "", bar: "hoo"})).to.be(undefined);
    expect(crossCheck("", {check: "bar", onlyWhenNoCheck: false, message: msg}, "2", {foo: "", bar: "hoo"})).to.be(msg);
    expect(crossCheck("", {check: "bar", onlyWhenNoCheck: true, message: msg}, "3", {foo: "", bar: undefined})).to.be(msg);
  });

  it("checks that checkNull option is working", function() {
    const msg = "msg";
    expect(crossCheck("", {check: "bar", onlyWhenCheckIn: [null],  checkNull: true, mustHaveValue: true, message: msg}, "1", {foo: "", bar: null})).to.be(msg);
    expect(crossCheck("", {check: "bar", onlyWhenCheckIn: [null],  checkNull: true, mustHaveValue: true, message: msg}, "1", {foo: "", bar: undefined})).to.be(msg);
    expect(crossCheck("", {check: "bar", onlyWhenCheckIn: [null],  checkNull: true, mustHaveValue: true, message: msg}, "1", {foo: ""})).to.be(msg);
    expect(crossCheck("", {check: "bar", onlyWhenCheckIn: [null],  checkNull: true, mustHaveValue: true, message: msg}, "1", {foo: "", bar: "hoo"})).to.be(undefined);
  });

  it("allows arbitrary nested validators", function() {
    const msg = "msg";
    const msg2 = "msg2";
    const value = null;
    const key = "dateEnd";
    const attributes = {dateEnd: "2021-01-01", dateBegin: "2022-02-02", foo:""};
    const innerValidation = {compareDate: {isAfter: "dateBegin", message: msg}, email: {message: msg2}};
    const options = {check: "bar", onlyWhenNoCheck: true, validators: innerValidation};
    const globalOptions = {fullMessages: false};
    expect(crossCheck(value, options, key, attributes, globalOptions)).to.eql([msg, msg2]);
  });

  it("checks that onlyWhenCheckEqualsValue works", function() {
    const msg = "msg";
    const key = "timeEnd";
    const attributes = {dateEnd: "2021-01-01", dateBegin: "2021-01-01", timeStart: "12:00", timeEnd: "11:59"};
    const value = attributes.timeEnd;
    const innerValidation = {compareNumerically: {greaterThan: "timeStart", message: msg}};
    const options = {check: "dateBegin", onlyWhenCheckEquals: "dateEnd", validators: innerValidation};
    const globalOptions = {fullMessages: false};
    expect(crossCheck(value, options, key, attributes, globalOptions, attributes)).to.be(msg);
  });

  it("checks that checkOptions with multiple onlyWhenCheckIn options is working", function() {
    const msg = "msg";
    const checkOptions = [{check: "bar", onlyWhenCheckIn: ["hoo"]}, {check: "baz", onlyWhenCheckIn: ["huu"]}];

    expect(crossCheck("", {check: "bar", checkOptions, message: msg}, "1", {foo: "", bar: "hoo", baz: "hoo"})).to.be(undefined);
    expect(crossCheck("", {check: "bar", checkOptions, message: msg}, "2", {foo: "", bar: "huu", baz: "huu"})).to.be(undefined);
    expect(crossCheck("", {check: "bar", checkOptions, message: msg}, "2", {foo: "", bar: "hoo", baz: "huu"})).to.be(msg);
    expect(crossCheck("", {check: "bar", checkOptions, mustBeEmpty: true, message: msg}, "3", {foo: "", bar: "hoo", baz: "huu"})).to.be(undefined);
  });
});
