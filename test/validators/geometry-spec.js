import {expect, lajiValidate} from "../bootstrap";

describe("validators.geometry", function () {

  var geometry = lajiValidate.validators.geometry.bind(lajiValidate.validators.geometry);

  it("allows valid geometries", function () {
    expect(geometry(undefined)).to.be(undefined);
    expect(geometry({})).to.be(undefined);
    expect(geometry({
      "type": "Point",
      "coordinates": [-180, 90]
    })).to.be(undefined);
    expect(geometry({
      "type": "Point",
      "radius": 200,
      "coordinates": [30, 60.230]
    })).to.be(undefined);
    expect(geometry({
      "type": "GeometryCollection",
      "geometries": [
        {
          "type": "Point",
          "radius": 200,
          "coordinates": [30, 60.230]
        },
        {
          "type": "Polygon",
          "coordinates": [[[-67.13734351262877, 45.137451890638886],
            [-66.96466, 44.8097],
            [-68.03252, 44.3252],
            [-69.06, 43.98],
            [-70.11617, 43.68405],
            [-67.79141211614706, 45.702585354182816],
            [-67.13734351262877, 45.137451890638886]]]
        }
      ]
    })).to.be(undefined);
    expect(geometry({
      "type": "GeometryCollection",
      "geometries": [
      ]
    })).to.be(undefined);
  });

  it("doesn't allow invalid geometries", function () {
    const msg = "invalid";
    expect(geometry({"type": "Point", "coordinates": [60.230]}, {message: {"invalidCoordinates": msg}})).to.be(msg);
    expect(geometry({"coordinates": []}, {message: msg})).to.be(msg);
    expect(geometry({"type": "Point", "coordinates": [90, 180]})).not.be(undefined);
    expect(geometry({"coordinates": [60.230, 30]}, {message: msg})).to.be(msg);
    expect(geometry({"type": "Point", "coordinates": [30, 60.230], nonExisting: false}, {message: msg})).to.be(msg);
    expect(geometry("foobar", {message: msg})).to.be(msg);
    expect(geometry(null, {message: msg})).to.be(msg);
    expect(geometry(123, {message: msg})).to.be(msg);
    expect(geometry(() => "test", {message: msg})).to.be(msg);

    // Should fail because radius is string
    expect(geometry({
      "type": "Point",
      "radius": "123",
      "coordinates": [30, 60.230]
    })).not.be(undefined);
    expect(geometry({
      "type": "GeometryCollection",
      "coordinates": [30, 60.230]
    })).not.be(undefined);
    expect(geometry({
      "type": "GeometryCollection",
      "geometries": [
      ]
    }, {requireShape: true})).not.be(undefined);
    expect(geometry({}, {requireShape: true})).not.be(undefined);
  });

  it("doesn't mutate value", function() {
    const goeJson = {
      "type": "GeometryCollection",
      "geometries": [
        {
          "type": "Point",
          "radius": 200,
          "coordinates": [30, 60.230]
        }
      ]
    };
    const original = JSON.parse(JSON.stringify(goeJson));
    expect(geometry(goeJson)).to.be(undefined);
    expect(goeJson).to.be.eql(original);
  });

  describe("overlapWith", function() {
    const msg = "no overlap";
    const area =  {
      "type": "Polygon",
      "coordinates": [[[-67.13734351262877, 45.137451890638886],
        [-66.96466, 44.8097],
        [-68.03252, 44.3252],
        [-69.06, 43.98],
        [-70.11617, 43.68405],
        [-67.79141211614706, 45.702585354182816],
        [-67.13734351262877, 45.137451890638886]]]
    };

    it("Shows error when no overlap with point", function() {
      const noOverlap = {
        "type": "Point",
        "coordinates": [-70.11617, 43.68404]
      };
      expect(geometry(noOverlap, {overlapWith: area, message: msg})).be(msg);
    });

    it("Shows error when no overlap with polygon", function() {
      const noOverlap = {
        "type": "Polygon",
        "coordinates": [[[-20.13734351262877, 45.137451890638886],
          [-20.96466, 44.8097],
          [-22.03252, 44.3252],
          [-23.06, 43.98],
          [-24.11617, 43.68405],
          [-20.79141211614706, 45.702585354182816],
          [-20.13734351262877, 45.137451890638886]]]
      };
      expect(geometry(noOverlap, {overlapWith: area, message: msg})).be(msg);
    });

    it("Doesn't show error when overlap exists with point", function() {
      const noOverlap = {
        "type": "Point",
        "coordinates": [-68.13334351262877, 44.933451890638886]
      };
      expect(geometry(noOverlap, {overlapWith: area, message: msg})).be(undefined);
    });

    it("Doesn't show error when overlap exists with point with radius", function() {
      const noOverlap = {
        "type": "Point",
        "radius": 100,
        "coordinates": [-70.11617, 43.68404]
      };
      expect(geometry(noOverlap, {overlapWith: area, message: msg})).be(undefined);
    });

    it("Empty doesn't overlap", function() {
      const noOverlap = {
        "type": "GeometryCollection",
        "geometries": []
      };
      expect(geometry(noOverlap, {overlapWith: area, message: msg})).be(undefined);
    });
  });

  describe("boundingBoxMaxHectares", function() {
    const msg = "too big area";
    const max  = 100000 * 100000 / 10000; // 100km x 100km as hectares
    const options =  {boundingBoxMaxHectares: max, message: {invalidBoundingBoxHectares: msg}};
    const outsideBoundingBoxUnits = [
      {
        unitGathering: {
          geometry: {
            type: "Point",
            coordinates: [23.30122746249336, 60.394385207839754]
          }
        }
      }
    ];
    const insideBoundingBoxUnits = [
      {
        unitGathering: {
          geometry: {
            type: "Point",
            coordinates: [23.338055, 60.167126]
          }
        }
      }
    ];
    const moreThanBoundingBox = [
      [
        23.30122746249336,
        60.394385207839754
      ],
      [
        25.25558542404245,
        60.438941038699085
      ],
      [
        25.329871055886837,
        59.439741231695585
      ],
      [
        23.362966794545276,
        59.42045098156848
      ],
      [
        23.30122746249336,
        60.394385207839754
      ]
    ];
    const lessThanBoundingBox = [
      [
        23.438058490461334,
        60.33369671381988
      ],
      [
        25.110819850379233,
        60.36799151054126
      ],
      [
        25.13457655030049,
        59.542872850498696
      ],
      [
        23.521074342508925,
        59.50759764017587
      ],
      [
        23.438058490461334,
        60.33369671381988
      ]
    ];

    it("doesn't care about point without radius", function() {
      expect(geometry({"type": "Point", "coordinates": lessThanBoundingBox[0]}, options)).to.be(undefined);
    });

    it("doesn't allow too big area for Point with radius", function() {
      expect(geometry({"type": "Point", "coordinates": lessThanBoundingBox[0], radius: 100000}, options)).to.be(msg);
    });

    it("allows Point with small radius", function() {
      expect(geometry({"type": "Point", "coordinates": lessThanBoundingBox[0], radius: 10}, options)).to.be(undefined);
    });

    it("doesn't allow too big area for Geometry Collection", function() {
      expect(geometry({
        type: "GeometryCollection",
        geometries: moreThanBoundingBox.map(coordinates => {
          return {type: "Point", coordinates};
        })
      }, options)).to.be(msg);
    });

    it("allows small area for Geometry Collection", function() {
      expect(geometry({
        type: "GeometryCollection",
        geometries: lessThanBoundingBox.map(coordinates => {
          return {type: "Point", coordinates};
        })
      }, options)).to.be(undefined);
    });

    it("doesn't allow too big area for Polygon", function() {
      expect(geometry({
        type: "Polygon",
        coordinates: [moreThanBoundingBox]
      }, options)).to.be(msg);
    });

    it("allows small area for Polygon", function() {
      expect(geometry({
        type: "Polygon",
        coordinates: [lessThanBoundingBox]
      }, options)).to.be(undefined);
    });

    it("doesn't allow too big bounding box area for LineString", function() {
      expect(geometry({
        type: "LineString",
        coordinates: moreThanBoundingBox
      }, options)).to.be(msg);
    });

    it("allows small area for LineString", function() {
      expect(geometry({
        type: "LineString",
        coordinates: lessThanBoundingBox
      }, options)).to.be(undefined);
    });

    it("allows collection with circle if inside max bound", function() {
      expect(geometry({
        type: "GeometryCollection",
        geometries: lessThanBoundingBox.map(coordinates => {
          return {type: "Point", radius: 100, coordinates};
        })
      }, options)).to.be(undefined);
    });

    it("doesn't allow collection with circle radius outside max bounds", function() {
      expect(geometry({
        type: "GeometryCollection",
        geometries: lessThanBoundingBox.map(coordinates => {
          return {type: "Point", radius: 100000, coordinates};
        })
      }, options)).to.be(msg);
    });

    it("allows collection with unitGathering inside max bounds", function() {
      expect(geometry({
        type: "GeometryCollection",
        geometries: lessThanBoundingBox.map(coordinates => {
          return {type: "Point", radius: 100, coordinates};
        })
      }, {...options, includeGatheringUnits: true}, "geometry", {units: insideBoundingBoxUnits})).to.be(undefined);
    });

    it("doesn't allow collection with unitGathering outside max bounds", function() {
      expect(geometry({
        type: "GeometryCollection",
        geometries: lessThanBoundingBox.map(coordinates => {
          return {type: "Point", radius: 100, coordinates};
        })
      }, {...options, includeGatheringUnits: true}, "geometry", {units: outsideBoundingBoxUnits})).to.be(msg);
    });

    it("formats key and max to message", function() {
      const options =  {boundingBoxMaxHectares: max, message: {invalidBoundingBoxHectares: msg}};
      expect(geometry({
        type: "GeometryCollection",
        geometries: lessThanBoundingBox.map(coordinates => {
          return {type: "Point", radius: 100000, coordinates};
        })
      }, {...options, message: {invalidBoundingBoxHectares: "%{key} %{key} on liian iso (maksimi %{max} %{max})"}}, "geomKey")).to.be(`geomKey geomKey on liian iso (maksimi ${max} ${max})`);
    });
  });

  describe("lines length", function() {
    const msg = "message %{minLength}";
    const options =  {minLineLength: 5, message: {minLineLength: msg}};

    const belowMinLength = {
      type: "LineString",
      coordinates: [
        [
          21.157287504315256,
          60.34655421503672
        ],
        [
          21.157278709763645,
          60.34653881947028
        ]
      ]
    };

    const overMinLength = {
      type: "LineString",
      coordinates: [
        [
          21.157287504315256,
          60.34655421503672
        ],
        [
          21.157289426602933,
          60.346483146497775
        ]
      ]
    };

    it("allows geometry with lines length more than minimum", function() {
      expect(geometry(overMinLength, options)).to.be(undefined);
    });

    it("doesn't allow geometry with lines length less than minimum", function() {
      expect(geometry(belowMinLength, options)).to.be("message 5");
    });

  });

  describe("minAreaHectares", function() {
    const msg = "message %{min}";
    const options =  {areaMinHectares: 1, message: {areaMinHectares: msg}};

    const belowMinArea = {
      type: "Polygon",
      coordinates: [
        [
          [
            25.557779,
            63.611612
          ],
          [
            25.559452,
            63.611612
          ],
          [
            25.559452,
            63.61254
          ],
          [
            25.557779,
            63.61254
          ],
          [
            25.557779,
            63.611612
          ]
        ]
      ]
    };

    const overMinArea = {
      type: "Polygon",
      coordinates: [
        [
          [
            25.556519,
            63.611817
          ],
          [
            25.55811,
            63.610474
          ],
          [
            25.55948,
            63.611285
          ],
          [
            25.558304,
            63.612764
          ],
          [
            25.556519,
            63.611817
          ]
        ]]
    };

    it("allows geometry with area more than minimum", function() {
      expect(geometry(overMinArea, options)).to.be(undefined);
    });

    it("doesn't allow geometry with area less than minimum", function() {
      expect(geometry(belowMinArea, options)).to.be("message 1");
    });

  });

  describe("minDistanceWith", function() {
    const msg = "message";

    const point = {
      "type": "Point",
      "coordinates": [
        23.845628,
        61.577225
      ]
    };
    const overMinDistance = {
      "type": "Point",
      "coordinates": [
        23.849957,
        61.576221
      ]
    };
    const belowMinDistance = {
      "type": "Point",
      "coordinates": [
        23.848159,
        61.576693
      ]
    };
    const options =  {minDistanceWith: point, minDistance: 200, message: {tooClose: msg}};

    it("allows geometry with distance more than minimum", function() {
      expect(geometry(overMinDistance, options)).to.be(undefined);
    });

    it("doesn't allow geometry with distance less than minimum", function() {
      expect(geometry(belowMinDistance, options)).to.be(msg);
    });

  });

  describe("ykjGrid", function() {
    const inside668_338 = {
      "type": "Polygon",
      "coordinates": [
        [
          [
            24.875605,
            60.236113
          ],
          [
            24.987713,
            60.236113
          ],
          [
            24.987713,
            60.284928
          ],
          [
            24.875605,
            60.284928
          ],
          [
            24.875605,
            60.236113
          ]
        ]
      ]
    };

    const coordinateVerbatim668_338 = {
      "type": "Polygon",
      "coordinates": [
        [
          [
            24.832165,
            60.213812
          ],
          [
            25.01243,
            60.216637
          ],
          [
            25.006982,
            60.306336
          ],
          [
            24.826223,
            60.303501
          ],
          [
            24.832165,
            60.213812
          ]
        ]
      ],
      "coordinateVerbatim": "668:338"
    };

    const polygonOverlapsToNorthOf338_668 = {
      "type": "Polygon",
      "coordinates": [
        [
          [
            24.87205,
            60.265007
          ],
          [
            24.973243,
            60.265007
          ],
          [
            24.973243,
            60.31354
          ],
          [
            24.87205,
            60.31354
          ],
          [
            24.87205,
            60.265007
          ]
        ]
      ]
    };

    const polygonOverlapsToEastOf338_668 = {
      "type": "Polygon",
      "coordinates": [
        [
          [
            24.969981,
            60.23542
          ],
          [
            25.021648,
            60.23542
          ],
          [
            25.021648,
            60.268347
          ],
          [
            24.969981,
            60.268347
          ],
          [
            24.969981,
            60.23542
          ]
        ]
      ]
    };

    const pointOutside338_668 = {
      "type": "Point",
      "coordinates": [
        24.039903,
        60.114434
      ]
    };

    const msg = "test";
    const options = {
      ykjOverlap: true,
      message: {
        ykjOverlap: msg
      }
    };


    const asGeometryCollection = (...geometries) => ({
      type: "GeometryCollection",
      geometries
    });


    it("allows geometry inside an ykj grid", function() {
      expect(geometry(inside668_338, options)).to.be(undefined);
    });

    it("allows line string", function() {
      expect(geometry({type: "LineString", coordinates: inside668_338.coordinates[0]}, options)).to.be(undefined);
    });

    it("allows multi line string", function() {
      expect(geometry({type: "MultiLineString", coordinates: inside668_338.coordinates}, options)).to.be(undefined);
    });


    it("allows point", function() {
      expect(geometry(pointOutside338_668, options)).to.be(undefined);
    });

    it("allows ykj grid overlapping if it has coordinateVerbatim", function() {
      expect(geometry(coordinateVerbatim668_338, options)).to.be(undefined);
    });

    it("allows multi geometry with ykj grid overlapping if it has coordinateVerbatim and others inside same", function() {
      expect(geometry(asGeometryCollection(coordinateVerbatim668_338, inside668_338), options)).to.be(undefined);
    });

    it("doesn't allow geometry overlapping ykj grids", function() {
      expect(geometry(polygonOverlapsToEastOf338_668, options)).to.be(msg);
      expect(geometry(polygonOverlapsToNorthOf338_668, options)).to.be(msg);
    });

    it("doesn't allow geometry overlapping ykj grid when multiple geometries", function() {
      expect(geometry(asGeometryCollection(coordinateVerbatim668_338, polygonOverlapsToNorthOf338_668), options)).to.be(msg);
      expect(geometry(asGeometryCollection(coordinateVerbatim668_338, pointOutside338_668), options)).to.be(msg);
    });
  });
});
