import {lajiValidate, expect} from "../bootstrap";

describe("validators.compareNumerically", function() {

  const {compareNumerically} = lajiValidate.validators;
  const context = {
    num1: 1,
    num2: "02",
    num3: "ab3c",
    num4: "foo3.4",
    num5: "-5",
  };
  context.nested = context;

  it("allows empty value", function() {
    expect(compareNumerically(undefined, {}, "foo", context)).to.be(undefined);
    expect(compareNumerically("", {}, "foo", context)).to.be(undefined);
  });

  it("allows valid inputs", function() {
    expect(compareNumerically("-3", {
      lessThan: "num2"
    }, "foo", context)).to.be(undefined);
    expect(compareNumerically("-3", {
      greaterThan: "num5"
    }, "foo", context)).to.be(undefined);
    expect(compareNumerically("3.4", {
      greaterThanOrEqualTo: "num4"
    }, "foo", context)).to.be(undefined);
    expect(compareNumerically("3.4", {
      lessThanOrEqualTo: "num4"
    }, "foo", context)).to.be(undefined);
    expect(compareNumerically("01", {
      lessThanOrEqualTo: "num1"
    }, "foo", context)).to.be(undefined);
    expect(compareNumerically("02.99991", {
      lessThan: "num3"
    }, "foo", context)).to.be(undefined);
    expect(compareNumerically(3.400001, {
      lessThanOrEqualTo: "num4",
      onlyWhenAttrIn: {
        num3: ["ab4c"]
      }
    }, "foo", context)).to.be(undefined);
  });

  it("doesn't allow invalid inputs", function() {
    const msg = "msg";
    expect(compareNumerically("1", {
      lessThan: "num1",
      message: msg
    }, "foo", context)).to.be(msg);
    expect(compareNumerically("-5", {
      greaterThan: "num5",
      message: msg
    }, "foo", context)).to.be(msg);
    expect(compareNumerically("0.200", {
      greaterThan: "num2",
      message: msg
    }, "foo", context)).to.be(msg);
    expect(compareNumerically("3.0", {
      lessThan: "num3",
      message: msg
    }, "foo", context)).to.be(msg);
    expect(compareNumerically("3.3999", {
      greaterThanOrEqualTo: "num4",
      message: msg
    }, "foo", context)).to.be(msg);
    expect(compareNumerically(3.400001, {
      lessThanOrEqualTo: "num1",
      message: msg,
      onlyWhenAttrIn: {
        num3: ["ab3c"]
      }
    }, "foo", context)).to.be(msg);
  });

  it("allows comparison to multiple valid fields", function() {
    expect(compareNumerically(3.0000, {
      lessThanOrEqualTo: "num3",
      lessThan: "num4",
      greaterThan: "num5",
      greaterThanOrEqualTo: "num3"
    }, "foo", context)).to.be(undefined);
  });

  it("allows comparison to sum of array", function () {
    let msg = "msg";
    expect(compareNumerically(2, {
      lessThan: ["num1", "num2"],
    }, "foo", context)).to.be(undefined);
    expect(compareNumerically("-4", {
      greaterThan: ["num5", "num1"],
      message: msg
    }, "foo", context)).to.be(msg);
  });

  it("works for json pointers", function() {
    expect(compareNumerically("-3", {
      lessThan: "/nested/num2"
    }, "foo", context)).to.be(undefined);
    expect(compareNumerically("-3", {
      greaterThan: "/nested/num5"
    }, "foo", context)).to.be(undefined);
    expect(compareNumerically("3.4", {
      greaterThanOrEqualTo: "/nested/num4"
    }, "foo", context)).to.be(undefined);
    expect(compareNumerically("3.4", {
      lessThanOrEqualTo: "/nested/num4"
    }, "foo", context)).to.be(undefined);
    expect(compareNumerically("01", {
      lessThanOrEqualTo: "/nested/num1"
    }, "foo", context)).to.be(undefined);
    expect(compareNumerically("02.99991", {
      lessThan: "/nested/num3"
    }, "foo", context)).to.be(undefined);
    expect(compareNumerically(3.400001, {
      lessThanOrEqualTo: "/nested/num4",
      onlyWhenAttrIn: {
        "/nested/num3": ["ab4c"]
      }
    }, "foo", context)).to.be(undefined);

    const msg = "msg";
    expect(compareNumerically("1", {
      lessThan: "/nested/num1",
      message: msg
    }, "foo", context)).to.be(msg);
    expect(compareNumerically("-5", {
      greaterThan: "/nested/num5",
      message: msg
    }, "foo", context)).to.be(msg);
    expect(compareNumerically("0.200", {
      greaterThan: "/nested/num2",
      message: msg
    }, "foo", context)).to.be(msg);
    expect(compareNumerically("3.0", {
      lessThan: "/nested/num3",
      message: msg
    }, "foo", context)).to.be(msg);
    expect(compareNumerically("3.3999", {
      greaterThanOrEqualTo: "/nested/num4",
      message: msg
    }, "foo", context)).to.be(msg);
    expect(compareNumerically(3.400001, {
      lessThanOrEqualTo: "/nested/num1",
      message: msg,
      onlyWhenAttrIn: {
        "/nested/num3": ["ab3c"]
      }
    }, "foo", context)).to.be(msg);
  });
});
