import {lajiValidate, expect} from "../bootstrap";

describe("validators.arrayDuplicateCheck", function() {

  const {arrayDuplicateCheck} = lajiValidate.validators;

  it("allows empty object", function () {
    expect(arrayDuplicateCheck(undefined, {}, "foo", undefined)).to.be(undefined);
    expect(arrayDuplicateCheck([], {}, "foo", {"foo": []})).to.be(undefined);
  });

  it("allows valid data", function () {
    expect(arrayDuplicateCheck(["a", "b"], {}, "foo", {"foo": ["a", "b"]})).to.be(undefined);
    expect(arrayDuplicateCheck(["a", "b"], {checkFields: ["bar"]}, "foo", {"foo": ["a", "b"], "bar": ["c", "d"]})).to.be(undefined);
  });

  it("doesn't allow duplicates", function () {
    const msg = "msg";
    expect(arrayDuplicateCheck(["a", "b", "b"], {message: msg})).to.be(msg);
    expect(arrayDuplicateCheck(["a", "b"], {checkFields: ["bar"], message: msg}, "foo", {"foo": ["a", "b"], "bar": ["c", "a"]})).to.be(msg);
  });
});
