import {lajiValidate, expect} from "../bootstrap";

describe("validators.duplicateCheck", function() {

  const {duplicateCheck} = lajiValidate.validators;

  it("allows empty object", function () {
    expect(duplicateCheck(undefined, {
      key: "bar",
      arrayPath: "foo"
    }, undefined, {"foo": [{"bar": 0}]}, undefined, {})).to.be(undefined);
    expect(duplicateCheck({}, {key: "bar", arrayPath: "foo"})).to.be(undefined);
  });

  it("allows valid simple data", function () {
    expect(duplicateCheck({"bar": 2}, {
      key: "bar",
      arrayPath: "foo"
    }, undefined, {"foo": [{"bar": 0}, {"bar": 1}, {"bar": 2}]}, undefined, {})).to.be(undefined);
    expect(duplicateCheck({"bar": "a"}, {
      key: "bar",
      arrayPath: "foo"
    }, undefined, {"foo": [{"bar": "a"}, {"bar": "b"}, {"bar": "c"}]}, undefined, {})).to.be(undefined);
  });

  it("doesn't allow duplicates", function () {
    const msg = "msg";
    expect(duplicateCheck({"bar": 2}, {
      key: "bar",
      arrayPath: "foo",
      message: msg
    }, undefined, {"foo": [{"bar": 2}, {"bar": 2}, {"bar": 1}]}, undefined, {})).to.be(msg);
    expect(duplicateCheck({"bar": "c"}, {
      key: "bar",
      arrayPath: "#/foo",
      message: "msg"
    }, undefined, {}, undefined, {"foo": [{"bar": "c"}, {"bar": "c"}]})).to.be(msg);
  });

  it("it checks relative path within an array", function () {
    const data = [
      {
        foo: [
          {identifications: [{taxonID: "parus"}]},
          {identifications: [{taxonID: "buffo buffo"}]},
          {identifications: [{taxonID: "parus"}]}
        ]
      }
    ];
    expect(duplicateCheck(
      data[0].foo[0],
      {key: "/identifications/0/taxonID", arrayPath: "../"},
      undefined,
      {},
      undefined,
      data
    )).to.be(undefined);
  });


  it("it checks relative path within an document and doesn't allow duplicates", function () {
    const msg = "msg";
    const data = {
      gatherings: [
        {
          units: [
            {identifications: [{taxonID: "parus"}]},
            {identifications: [{taxonID: "buffo buffo"}]},
            {identifications: [{taxonID: "parus"}]}
          ]
        }
      ]
    };
    expect(duplicateCheck(
      data.gatherings[0].units[0],
      {key: "/identifications/0/taxonID", arrayPath: "../units", message: msg},
      undefined,
      data.gatherings[0].units,
      undefined,
      data
    )).to.be(msg);
  });

  it("uses messageKey", function () {
    const msg = "msg %{value}";
    const data = {
      gatherings: [
        {
          units: [
            {identifications: [{taxonID: "parus", foo: "bar"}]},
            {identifications: [{taxonID: "parus", foo: "bar"}]}
          ]
        }
      ]
    };
    expect(duplicateCheck(
      data.gatherings[0].units[0],
      {key: "/identifications/0/taxonID", messageKey: "/identifications/0/foo", arrayPath: "../units", message: msg},
      undefined,
      data.gatherings[0].units,
      undefined,
      data
    )).to.be(msg.replace("%{value}", "bar"));
  });

  it("it checks relative path within an document", function () {
    const data = {
      gatherings: [
        {
          units: [
            {identifications: [{taxonID: "parus"}]},
            {identifications: [{taxonID: "buffo buffo"}]},
            {identifications: [{taxonID: "parus major"}]}
          ]
        }
      ]
    };
    expect(duplicateCheck(
      data.gatherings[0].units[0],
      {key: "/identifications/0/taxonID", arrayPath: "../units"},
      undefined,
      data.gatherings[0].units,
      undefined,
      data
    )).to.be(undefined);
  });

  it("it works with whole schema", function () {
    const msg = "msg";
    const schema = {
      gatherings: {
        items: {
          properties: {
            units: {
              items: {
                duplicateCheck: {
                  key: "/identifications/0/taxonID",
                  arrayPath: "../units",
                  message: msg
                }
              }
            }
          }
        }
      }
    };
    const data = {
      gatherings: [
        {
          units: [
            {identifications: [{taxonID: "parus"}]},
            {identifications: [{taxonID: "buffo buffo"}]},
            {identifications: [{taxonID: "parus"}]}
          ]
        }
      ]
    };
    expect(lajiValidate(data, schema)).to.eql({
      "gatherings[0].units[0]": [msg],
      "gatherings[0].units[2]": [msg],
    });
  });

  it("it works with undefined in an array @pick", function () {
    const msg = "msg";
    const schema = {
      gatherings: {
        items: {
          properties: {
            units: {
              items: {
                duplicateCheck: {
                  key: "/identifications/0/taxonID",
                  arrayPath: "../units",
                  message: msg
                }
              }
            }
          }
        }
      }
    };
    const data = {
      gatheringEvent: {
        leg: [undefined]
      },
      gatherings: [
        {
          units: [
            {identifications: [{taxonID: "parus"}]},
            {identifications: [{taxonID: "buffo buffo"}]},
            {identifications: [{taxonID: "parus"}]}
          ]
        }
      ]
    };
    expect(lajiValidate(data, schema)).to.eql({
      "gatherings[0].units[0]": [msg],
      "gatherings[0].units[2]": [msg],
    });
  });

});
