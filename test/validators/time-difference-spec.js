import {lajiValidate, expect} from "../bootstrap";

describe("validators.timeDifference", function() {

  const {timeDifference} = lajiValidate.validators;


  it("it allows empty values", function() {
    expect(timeDifference("")).to.be(undefined);
    expect(timeDifference(undefined)).to.be(undefined);
    expect(timeDifference({})).to.be(undefined);
  });

  it("it allows valid difference with max difference", function() {
    expect(timeDifference("03:36", {compareTo: "time2", maxDifference: 70}, "time", {time: "03:36", time2: "03:35"})).to.be(undefined);
    expect(timeDifference("04:45", {compareTo: "time2", maxDifference: 70}, "time", {time: "04:35", time2: "03:35"})).to.be(undefined);
  });

  it("it doesn't allow difference bigger than max difference", function() {
    const msg = "msg";
    expect(timeDifference("04:46", {compareTo: "time2", maxDifference: 70, message: msg}, "time", {time: "04:46", time2: "03:35"})).to.be(msg);
    expect(timeDifference("03:50", {compareTo: "time2", maxDifference: 5, message: msg}, "time", {time: "03:50", time2: "03:35"})).to.be(msg);
  });

  it("it allows valid difference with min difference", function() {
    expect(timeDifference("03:36", {compareTo: "time2", minDifference: 1}, "time", {time: "03:36", time2: "03:35"})).to.be(undefined);
    expect(timeDifference("04:45", {compareTo: "time2", minDifference: 60}, "time", {time: "04:35", time2: "03:35"})).to.be(undefined);
  });

  it("it doesn't allow difference less than min difference", function() {
    const msg = "msg";
    expect(timeDifference("04:45", {compareTo: "time2", minDifference: 71, message: msg}, "time", {time: "04:45", time2: "03:35"})).to.be(msg);
    expect(timeDifference("03:50", {compareTo: "time2", minDifference: 120, message: msg}, "time", {time: "03:50", time2: "03:35"})).to.be(msg);
  });
});
