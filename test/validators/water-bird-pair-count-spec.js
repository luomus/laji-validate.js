import {lajiValidate, expect} from "../bootstrap";

describe("validators.waterBirdPairCount", function() {

  const {waterBirdPairCount: waterBirdPairCount} = lajiValidate.validators;

  it("allows valid pair count", function() {
    expect(waterBirdPairCount(4, {},  undefined, {shortHandText: "2k2n"})).to.be(undefined);
    expect(waterBirdPairCount(2, {}, undefined, {shortHandText: "Ä, 1"})).to.be(undefined);
    expect(waterBirdPairCount(10, {}, undefined, {shortHandText: "4n, 5k, ä"})).to.be(undefined);
  });

  it("allows valid pair count when no short hand text", function() {
    expect(waterBirdPairCount(11, {},  undefined, {})).to.be(undefined);
    expect(waterBirdPairCount(11, {},  undefined, {shortHandText: "dsaflkf"})).to.be(undefined);
  });

  it("doesn't allow invalid pair count", function() {
    const msg = "msg";
    expect(waterBirdPairCount(5, {message: msg}, undefined, {shortHandText: "2k2n"})).to.be(msg);
    expect(waterBirdPairCount(5, {message: msg}, undefined, {shortHandText: "2, k, Ä"})).to.be(msg);
  });

});
