import {lajiValidate, expect} from "../bootstrap";

describe("validators.compareDate", function() {

  const {compareDate} = lajiValidate.validators;

  it("allows valid date span", function() {
    expect(compareDate("2013-10-28", {isAfter: "date1"},  undefined, {date1: "2013-10-27"})).to.be(undefined);
    expect(compareDate("2013-10-26", {isBefore: "date1"}, undefined, {date1: "2013-10-27"})).to.be(undefined);
    expect(compareDate("2013-10-26", {isBefore: "date1", isAfter: "date2"}, undefined, {date1: "2013-10-27"})).to.be(undefined);
    expect(compareDate("2013-10-26", {isBefore: "date1", isAfter: "date2"}, undefined, {date1: "2013-10-27", date2: "2013-10-25"})).to.be(undefined);
  });

  it("allows valid date span with time", function() {
    expect(compareDate("2013-10-26T00:00:01+00:00", {isAfter: "date1"}, undefined, {date1: "2013-10-26T00:00:00+00:00"})).to.be(undefined);
    expect(compareDate("2013-10-26T00:00:00+00:00", {isBefore: "date1"}, undefined, {date1: "2013-10-26T00:00:01+00:00"})).to.be(undefined);
    expect(compareDate("2017-05-24T18:07", {isBefore: "date1"}, undefined, {date1: "2017-05-24T18:08"})).to.be(undefined);
    expect(compareDate("2013-10-28", {isBefore: "date1"}, undefined, {date1: "2013-10-28"})).to.be(undefined);
  });

  it("doesn't allow invalid date span", function() {
    const msg = "msg";
    expect(compareDate("2013-10-27", {isAfter: "date1", message: msg}, undefined, {date1: "2013-10-28"})).to.be(msg);
    expect(compareDate("2013-10-29", {isBefore: "date1", message: msg}, undefined, {date1: "2013-10-28"})).to.be(msg);
  });

  it("doesn't allow valid date span with time", function() {
    const msg = "msg";
    expect(compareDate("2013-10-26T00:00:00+00:00", {isAfter: "date1", message: msg}, undefined, {date1: "2013-10-26T00:00:01+00:00"})).to.be(msg);
    expect(compareDate("2013-10-26T00:00:01+00:00", {isBefore: "date1", message: msg}, undefined, {date1: "2013-10-26T00:00:00+00:00"})).to.be(msg);
  });

  it("allows dates with missing compare to field", function() {
    expect(compareDate("2013-10-28", {isAfter: "date1"},  undefined, {foo: "2013-10-27"})).to.be(undefined);
  });

  it("doesn't allow compare field missing time if has time", function() {
    const msg = "must have time if ${target} has.";
    const msgResolved = "must have time if date1 has.";
    expect(compareDate("2013-10-26", {hasTimeIfOtherHas: "date1", message: msg}, undefined, {date1: "2013-10-26T00:00:00+00:00"})).to.be(msgResolved);
  });

  it("allows compare field missing time if hasn't time", function() {
    const msg = "msg";
    expect(compareDate("2013-10-26", {hasTimeIfOtherHas: "date1", message: msg}, undefined, {date1: "2013-10-26"})).to.be(undefined);
  });

});
