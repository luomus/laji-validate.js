import {lajiValidate, expect} from "../bootstrap";

describe("validators.monthDayRange", function() {

  const {monthDayRange} = lajiValidate.validators;


  it("it allows empty values", function() {
    expect(monthDayRange("")).to.be(undefined);
    expect(monthDayRange(undefined)).to.be(undefined);
    expect(monthDayRange({})).to.be(undefined);
  });

  it("allows valid dates", function() {
    let options = {ranges: [{earliestMonth: 2, earliestDay: 2, latestMonth: 3, latestDay: 4}]};
    expect(monthDayRange("2013-03-02", options)).to.be(undefined);
    expect(monthDayRange("2978-02-02", options)).to.be(undefined);
    options = {ranges: [{earliestMonth: 11, earliestDay: 5, latestMonth: 3, latestDay: 20}]};
    expect(monthDayRange("1999-01-01", options)).to.be(undefined);
    expect(monthDayRange("2002-12-04", options)).to.be(undefined);
  });

  it("doesn't allow dates that are not in any range", function() {
    const message = "msg";
    let options = {message: message, ranges: [{earliestMonth: 2, earliestDay: 10, latestMonth: 3, latestDay: 10}, {earliestMonth: 3, earliestDay: 25, latestMonth: 7, latestDay: 7}]};
    expect(monthDayRange("2016-03-11", options)).to.be(message);
    expect(monthDayRange("2017-09-24", options)).to.be(message);
  });

  it("allows conditional validation based on another property", function() {
    const msg = "msg";
    let options = {ranges: [{earliestMonth: 2, earliestDay: 10, latestMonth: 3, latestDay: 10}, {earliestMonth: 3, earliestDay: 25, latestMonth: 7, latestDay: 7}], message: msg};
    expect(monthDayRange("2016-03-11", {...options, onlyWhenField: "foo", onlyWhenFieldIn: ["bar", "baa"]}, "bar", {"foo": "foo", "bar": "2013-03-02"})).to.be(undefined);
    expect(monthDayRange("2016-03-11", {...options, onlyWhenField: "foo", onlyWhenFieldIn: ["bar", "baa"]}, "bar", {"foo": "bar", "bar": "2013-03-02"})).to.be(msg);
  });
  it("allows conditional validation based on ykj north coordinate", function() {
    const msg = "msg";
    let options = {
      ranges: [{earliestMonth: 2, earliestDay: 10, latestMonth: 3, latestDay: 10}, {earliestMonth: 3, earliestDay: 25, latestMonth: 7, latestDay: 7}],
      message: msg
    };
    const geometry1 = {
      type: "Point",
      coordinates: [23.30122746249336, 60.394385207839754]
    };
    const geometry2 = {
      type: "Point",
      coordinates: [23.30122746249336, 62.594385207839754]
    };
    expect(monthDayRange("2016-03-11", {...options, onlyWhenYkjNorthCoordinateSmallerThan: 690}, "bar", {"geometry": geometry2, "bar": "2013-03-02"})).to.be(undefined);
    expect(monthDayRange("2016-03-11", {...options, onlyWhenYkjNorthCoordinateSmallerThan: 690}, "bar", {"geometry": geometry1, "bar": "2013-03-02"})).to.be(msg);
    expect(monthDayRange("2016-03-11", {...options, onlyWhenYkjNorthCoordinateBiggerThan: 690}, "bar", {"geometry": geometry1, "bar": "2013-03-02"})).to.be(undefined);
    expect(monthDayRange("2016-03-11", {...options, onlyWhenYkjNorthCoordinateBiggerThan: 690}, "bar", {"geometry": geometry2, "bar": "2013-03-02"})).to.be(msg);
  });
});
