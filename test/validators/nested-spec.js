import {lajiValidate, expect} from "../bootstrap";

describe("validator.items", function() {

  var items = lajiValidate.validators.items.bind(lajiValidate.validators.items);
  var properties = lajiValidate.validators.properties.bind(lajiValidate.validators.properties);

  describe("propagates options as sub-validations", function() {
    it("applies equality and inclusion validations to interior attributes", function() {
      var schema = {
        inclusion: {
          within: ["foo", "bar", "baz"],
          message: "%{value} is not in the list!"
        }
      };
      var val = ["foo", "bar", "three"];
      var res = items(val, schema);
      expect(res).to.eql({
        "[2]": [
          "three is not in the list!"
        ]
      });
    });
  });

  describe("supports multiple layers of nesting", function() {
    it("supports two layers of value constraints", function() {
      var schema = {
        "array": {
          items: {
            items: {
              inclusion: {
                within: ["validVal"]
              }
            }
          }
        }
      };
      var validCase = {
        array: [["validVal"], []]
      };
      var invalidCase = {
        array: [["invalidVal", "validVal", "two"], []]
      };
      expect(lajiValidate(validCase, schema)).to.be(undefined);
      expect(lajiValidate(invalidCase, schema)).to.eql({
        "array[0][0]": [
          "invalidVal is not included in the list"
        ],
        "array[0][2]": [
          "two is not included in the list"
        ]
      });
    });
  });

  describe("handles items inside arrays", function() {
    it("allows value specification targeting arrays", function() {
      var schema = {
        inclusion: {
          within: ["validVal"]
        }
      };
      expect(items(["validVal"], schema)).to.be(undefined);
      expect(items(["invalidVal"], schema)).to.be.an("object");
    });
  });

  describe("handles items inside objects", function() {
    it("allows value specification targeting objects", function() {
      var schema = {
        someKey: {
          inclusion: {
            within: ["validVal"]
          }
        }
      };
      expect(properties({someKey: "validVal"}, schema)).to.be(undefined);
      expect(properties({someKey: "invalidVal"}, schema)).to.be.an("object");
    });
  });

  describe("rejects things that are not arrays", function() {
    it("rejects things that are not arrays", function() {
      var schema = {
        inclusion: {
          within: ["validVal"]
        }
      };
      expect(items("notAnArray", schema)).to.eql(undefined);
    });
  });

  describe("rejects things that are not objects", function() {
    it("rejects things that are not object", function() {
      var schema = {
        inclusion: {
          within: ["validVal"]
        }
      };
      expect(properties("notAnArray", schema)).to.eql(undefined);
    });
  });

});
