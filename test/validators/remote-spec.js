import {lajiValidate, expect} from "../bootstrap";
import queryString from "querystring";
import merge from "deepmerge";
import fetch from "isomorphic-fetch";

const config = require("../config.json");

const wbcForm = "MHL.3";
const hitToExistingWBCCount = {
  id: "JX.128717",
  namedPlaceID: "MNP.21560",
  dateBegin: "25.12.2016"
};

lajiValidate.extend(lajiValidate.validators.remote, {
  fetch: (path, query, options) => {
    const baseQuery = config.baseQuery;
    const queryObject = (typeof query == "object") ? merge(baseQuery, query) : baseQuery;
    return fetch(`${config.apiBase}${path}?${queryString.stringify(queryObject)}`, options);
  }
});

describe("validators.remote", function() {

  it("Doesn't validate when no value", function(done) {
    const schema = {
      id: {
        remote: {

        }
      }
    };
    lajiValidate.async({"id": ""}, schema)
    .then(() => {
      done();
    })
    .catch(() => {
      done("Shold not throw error when validationg with no value");
    });
  });

  it("Gives an error when no form ID", function(done) {
    const schema = {
      id: {
        remote: {
          validator: "noExistingGatheringsInNamedPlace"
        }
      }
    };
    lajiValidate.async({"id": "foobar"}, schema)
      .then(() => {
        done("Should throw error when no form data present");
      })
      .catch(err => {
        expect(err).to.eql({"id": ["Missing required parameter formID"]});
        done();
      });
  });

  it("Gives an error when no namedPlaceID ID", function(done) {
    const schema = {
      gatheringEvent: {
        properties: {
          dateBegin: {
            remote: {
              validator: "noExistingGatheringsInNamedPlace"
            }
          }
        }
      }
    };
    const data = {
      id: "FooBar",
      formID: wbcForm,
      gatheringEvent: {
        dateBegin: "2017-10-01"
      }
    };
    lajiValidate.async(data, schema)
      .then(() => {
        done("Should throw error when no form data present");
      })
      .catch(err => {
        expect(err).to.eql({"gatheringEvent.dateBegin": ["Could not find any named place in the document"]});
        done();
      });
  });

  it("It doesn't allow data with invalid namedPlaceID ID", function(done) {
    const schema = {
      gatheringEvent: {
        properties: {
          dateBegin: {
            remote: {
              validator: "noExistingGatheringsInNamedPlace"
            }
          }
        }
      }
    };
    const data = {
      id: "FooBar",
      formID: wbcForm,
      gatheringEvent: {
        dateBegin: "2017-10-01"
      }
    };
    lajiValidate.async(data, schema)
    .then((res) => {
      expect(res).to.be(undefined);
      done();
    })
    .catch((err) => {
      expect(err).to.eql({"gatheringEvent.dateBegin": ["Could not find any named place in the document"]});
      done();
    });
  });

  it("It allows valid cases", function(done) {
    const schema = {
      gatheringEvent: {
        properties: {
          dateBegin: {
            remote: {
              validator: "noExistingGatheringsInNamedPlace"
            }
          }
        }
      }
    };
    const data = {
      id: "FooBar",
      formID: wbcForm,
      namedPlaceID: "MAN.1",
      gatheringEvent: {
        dateBegin: "2017-10-01"
      }
    };
    lajiValidate.async(data, schema)
    .then(() => {
      done();
    })
    .catch(() => {
      done("Should not throw error when data is correct");
    });
  });

  it("Gives an error when trying to send to already send period", function(done) {
    const schema = {
      gatheringEvent: {
        properties: {
          dateBegin: {
            remote: {
              path: ".gatheringEvent.dateBegin",
              validator: "noExistingGatheringsInNamedPlace"
            }
          }
        }
      }
    };
    const data = {
      id: "FooBar",
      formID: wbcForm,
      namedPlaceID: hitToExistingWBCCount.namedPlaceID,
      gatheringEvent: {
        dateBegin: hitToExistingWBCCount.dateBegin
      }
    };
    lajiValidate.async(data, schema)
    .then(() => {
      done("Should give an error when trying to send to existing period");
    })
    .catch((err) => {
      expect(err).to.eql({"gatheringEvent.dateBegin": ["Observation already exists withing the given gathering period."]});
      done();
    });
  });


  it("Valid if existing is same id than the one that exists", function(done) {
    const schema = {
      gatheringEvent: {
        properties: {
          dateBegin: {
            remote: {
              validator: "noExistingGatheringsInNamedPlace"
            }
          }
        }
      }
    };
    const data = {
      id: hitToExistingWBCCount.id,
      formID: wbcForm,
      namedPlaceID: hitToExistingWBCCount.namedPlaceID,
      gatheringEvent: {
        dateBegin: hitToExistingWBCCount.dateBegin
      }
    };
    lajiValidate.async(data, schema)
    .then(() => {
      done();
    })
    .catch(() => {
      done("It shouldn't give error when adding item with existing id");
    });
  });
});
