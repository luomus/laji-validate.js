import {lajiValidate, expect} from "../bootstrap";

describe("validators.time", function() {

  const {time} = lajiValidate.validators;


  it("it allows empty values", function() {
    expect(time("")).to.be(undefined);
    expect(time("2013-04-22")).to.be(undefined);
    expect(time(undefined)).to.be(undefined);
    expect(time({})).to.be(undefined);
  });

  it("allows valid time with earliest", function() {
    const options = {earliestHour: 3, earliestMinute: 34};
    expect(time("2013-10-01T04:04:04", options)).to.be(undefined);
    expect(time("2978-04-03 03:35", options)).to.be(undefined);
    expect(time("T03:35:00.000Z", options)).to.be(undefined);
    expect(time("03:35", options)).to.be(undefined);
    expect(time("04:05", options)).to.be(undefined);
  });

  it("doesn't allow time that is too early", function() {
    const message = "msg";
    const options = {earliestHour: 3, earliestMinute: 4, message: message};
    expect(time("T02:30:00.000Z3", options)).to.be(message);
    expect(time("2013-04-01 03:03", options)).to.be(message);
    expect(time("2013-04-01 02:30", options)).to.be(message);
  });

  it("allows valid time with latest", function() {
    const options = {latestHour: 3, latestMinute: 4};
    expect(time("01:50", options)).to.be(undefined);
    expect(time("03:00", options)).to.be(undefined);
  });

  it("doesn't allow time that is too late", function() {
    const message = "msg";
    const options = {latestHour: 3, latestMinute: 4, message: message};
    expect(time("03:05", options)).to.be(message);
    expect(time("04:01", options)).to.be(message);
  });

  it("earliest hour works with zero", function() {
    const message = "msg";
    const options = {earliestHour: 0, earliestMinute: 4, message: message};
    expect(time("00:00", options)).to.be(message);
  });

  it("earliest minute works with zero", function() {
    const message = "msg";
    const options = {earliestHour: 1, earliestMinute: 0, message: message};
    expect(time("00:03", options)).to.be(message);
  });

  it("latest hour works with zero", function() {
    const message = "msg";
    const options = {latestHour: 0, latestMinute: 4, message: message};
    expect(time("01:02", options)).to.be(message);
  });

  it("latest minute works with zero", function() {
    const message = "msg";
    const options = {latestHour: 1, latestMinute: 0, message: message};
    expect(time("10:03", options)).to.be(message);
  });

  it("doesn't allow time outside earliest and latest", function() {
    const message = "msg";
    const options = {earliestHour: 1, earliestMinute: 0, latestHour: 2, latestMinute: 0, message};
    expect(time("00:30", options)).to.be(message);
    expect(time("02:30", options)).to.be(message);
  });
});
