import {lajiValidate, expect} from "../bootstrap";

describe("validators.stringArray", function() {

  const {stringArray} = lajiValidate.validators;


  it("it allows empty values", function() {
    expect(stringArray(undefined)).to.be(undefined);
    expect(stringArray([])).to.be(undefined);
  });

  it("it doesn't allow empty values with mustNotBeEmpty option", function() {
    const msg = "msg";
    const options = {mustNotBeEmpty: true, message: msg};
    expect(stringArray(undefined, options)).to.be(msg);
    expect(stringArray([], options)).to.be(msg);
    expect(stringArray([undefined], options)).to.be(msg);
    expect(stringArray([""], options)).to.be(msg);
  });

  it("it allows valid non empty values with mustNotBeEmpty option", function() {
    const options = {mustNotBeEmpty: true};
    expect(stringArray(["a"], options)).to.be(undefined);
    expect(stringArray([undefined, "b"], options)).to.be(undefined);
    expect(stringArray(["c", ""], options)).to.be(undefined);
  });
});
