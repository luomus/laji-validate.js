import {lajiValidate, expect} from "../bootstrap";

describe("validators.valueInArray", function() {

  const {valueInArray} = lajiValidate.validators;

  it("allows valid simple data", function() {
    expect(valueInArray("haa", {arrayField: "bar"}, undefined, {"bar": ["haa"]})).to.be(undefined);
  });

  it("allows validating whether fixed value is in array", function() {
    const msg = "msg";
    expect(valueInArray("haa", {value: "hoo", arrayField: "bar", message: msg}, undefined, {"bar": ["hoo", "hee"]})).to.be(undefined);
    expect(valueInArray("haa", {value: "hoo", arrayField: "bar", message: msg}, undefined, {"bar": ["haa", "hee"]})).to.be(msg);
  });

  it("allows giving conditions", function() {
    const msg = "msg";
    const options = {
      value: "hee",
      arrayField: "bar",
      conditions: [{
        onlyWhenField: "baz",
        onlyWhenFieldIn: ["huu"]
      }],
      message: msg
    };
    expect(valueInArray("haa", options, undefined, {"bar": ["hoo"], "baz": "hee"})).to.be(undefined);
    expect(valueInArray("haa", options, undefined, {"bar": ["hoo"], "baz": "huu"})).to.be(msg);
  });

  it("allows giving multiple conditions", function() {
    const msg = "msg";
    const options = {
      value: "hee",
      arrayField: "bar",
      conditions: [{
        onlyWhenField: "baz",
        onlyWhenFieldIn: ["huu"]
      }, {
        onlyWhenField: "foo",
        onlyWhenFieldIn: ["hoo"]
      }],
      message: msg
    };
    expect(valueInArray("haa", options, undefined, {"bar": ["hoo"], "baz": "huu", "foo": undefined})).to.be(undefined);
    expect(valueInArray("haa", options, undefined, {"bar": ["hoo"], "baz": "huu", "foo": "hoo"})).to.be(msg);
  });

  it("allows validating array of objects by key", function() {
    const msg = "msg";
    const formData = {
      "bar": [{
        "key1": "huu",
        "key2": "hii"
      }, {
        "key1": "hoo"
      }]
    };
    let options = {value: "hii", arrayField: "bar", arrayFieldKey: "key1", message: msg};
    expect(valueInArray("haa", options, undefined, formData)).to.be(msg);

    options = {value: "hoo", arrayField: "bar", arrayFieldKey: "key1", message: msg};
    expect(valueInArray("haa", options, undefined, formData)).to.be(undefined);

    options = {value: "hii", arrayField: "bar", arrayFieldKey: "key2", message: msg};
    expect(valueInArray("haa", options, undefined, formData)).to.be(undefined);

    options = {value: "hoo", arrayField: "bar", arrayFieldKey: "key2", message: msg};
    expect(valueInArray("haa", options, undefined, formData)).to.be(msg);
  });
});
