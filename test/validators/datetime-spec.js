import {lajiValidate, expect} from "../bootstrap";
const moment = require("moment");

describe("validators.datetime", function() {

  var datetime = lajiValidate.validators.datetime.bind(lajiValidate.validators.datetime);

  it("allows valid dates", function() {
    expect(datetime("2013-10-26", {dateOnly: true})).to.be(undefined);
    expect(datetime("2013-10-26T13:47:00", {})).to.be(undefined);
    expect(datetime("2013-10-26T13:47:00+02:00", {})).to.be(undefined);
    expect(datetime("2013-10-26 13:47:00+02:00", {})).to.be(undefined);
  });

  it("doesn't allow invalid dates", function() {
    expect(datetime("2013-13-06", {dateOnly: true})).to.be("must be a valid date");
    expect(datetime("2013-10-26M13:47:00+02:00", {})).to.be("must be a valid date");
  });

  it("doesn't allow tooLate and tooEarly", function() {
    var options = {
      earliest: "2015-01-01",
      latest: "nowExact",
      dateOnly: true
    };
    var now = new Date();
    var nowStr = now.toISOString().slice(0,10);
    expect(datetime("2216-01-01", options))
      .to.contain("must be no later than " + nowStr);
    expect(datetime("2014-12-31", options))
      .to.contain("must be no earlier than 2015-01-01");
  });

  it("allow dates when earliest and latest options are given", function() {
    var options = {
      earliest: "2015-01-01",
      latest: "nowExact",
      dateOnly: true
    };
    var now = new Date();
    var nowStr = now.toISOString().slice(0,10);
    expect(datetime(nowStr, options)).to.be(undefined);
    expect(datetime("2015-01-01", options)).to.be(undefined);
  });

  it("allow dates when close to current time", function() {
    var nowStr = moment().format("YYYY-MM-DD HH:mm:ss");
    var options = {
      latest: "nowExact",
      message: nowStr
    };
    expect(datetime(nowStr, options)).to.be(undefined);
  });

  it("allow dates to be within 12h of now", function() {
    var nowStr = moment().add(12, "hours").format("YYYY-MM-DD HH:mm:ss");
    var options = {
      latest: "now",
      message: nowStr
    };
    expect(datetime(nowStr, options)).to.be(undefined);
  });

  it("doesn't allow dates to be more than 12h of now", function() {
    var nowStr = moment().add(13, "hours").format("YYYY-MM-DD HH:mm:ss");
    var options = {
      latest: "now",
      message: nowStr
    };
    expect(datetime(nowStr, options)).to.contain(nowStr);
  });

  it("allow dates to be compared to last month", function() {
    var nowStr = moment().format("YYYY-MM-DD HH:mm:ss");
    var options = {
      earliest: "lastMonth",
      message: nowStr,
      dateOnly: false
    };
    expect(datetime(nowStr, options)).to.be(undefined);
    const dateStr = moment().subtract(2, "months").format("YYYY-MM-DD HH:mm:ss");
    options = {
      earliest: "lastMonth",
      message: dateStr,
      dateOnly: false
    };
    expect(datetime(dateStr, options)).to.contain(dateStr);
  });

  it("allow dates to be compared to last six months", function() {
    var nowStr = moment().format("YYYY-MM-DD HH:mm:ss");
    var options = {
      earliest: "lastSixMonths",
      message: nowStr,
      dateOnly: false
    };
    expect(datetime(nowStr, options)).to.be(undefined);
    const dateStr = moment().subtract(7, "months").format("YYYY-MM-DD HH:mm:ss");
    options = {
      earliest: "lastSixMonths",
      message: dateStr,
      dateOnly: false
    };
    expect(datetime(dateStr, options)).to.contain(dateStr);
  });

  it("gives different error message on notValid", function() {
    const msg = "correct error message";
    const options = {
      earliest: "lastMonth",
      notValid: msg,
      tooEarly: "foo",
      tooLate: "bar",
      dateOnly: false
    };
    expect(datetime("invalid", options)).to.contain(msg);
  });
});
