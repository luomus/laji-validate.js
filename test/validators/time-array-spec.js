import {lajiValidate, expect} from "../bootstrap";

describe("validators.time-array", function() {

  const {timeArray} = lajiValidate.validators;

  describe("order", function() {
    const message = "msg";
    const options = {
      order: true,
      message: {
        order: message
      }
    };

    it("allows chronological times", function() {
      expect(timeArray([
        {timeStart: "00:00"},
        {timeStart: "00:01"},
        {timeStart: "00:02"}
      ], options)).to.be(undefined);
    });

    it("doesn't allow nonchronological times", function() {
      expect(timeArray([
        {timeStart: "00:03"},
        {timeStart: "00:01"},
        {timeStart: "00:02"}
      ], options)).to.be(message);
    });

    it("allows adjacent equal times", function() {
      expect(timeArray([
        {timeStart: "00:01"},
        {timeStart: "00:01"},
        {timeStart: "00:02"}
      ], options)).to.be(undefined);
    });

    it("allows items missing time", function() {
      expect(timeArray([
        {timeStart: "00:01"},
        {},
        {timeStart: "00:02"}
      ], options)).to.be(undefined);
    });

    it("works for other fields than timeStart", function() {
      expect(timeArray([
        {timeEnd: "00:01"},
        {timeEnd: "00:00"}
      ], {...options, field: "timeEnd"})).to.be(message);
    });
  });

  describe("count", function() {
    const message = "msg";
    const options = {
      count: 2,
      message: {
        count: message
      }
    };

    it("doesn't allow count lesser than limit", function() {
      expect(timeArray([
        {timeStart: "00:00"},
      ], options)).to.be(message);
    });

    it("allows count equal to limit", function() {
      expect(timeArray([
        {timeStart: "00:00"},
        {timeStart: "00:01"}
      ], options)).to.be(undefined);
    });

    it("allows count more than limit", function() {
      expect(timeArray([
        {timeStart: "00:00"},
        {timeStart: "00:01"},
        {timeStart: "00:02"}
      ], options)).to.be(undefined);
    });

    it("Works for other fields than timeEnd", function() {
      const _options = {
        ...options,
        field: "timeEnd"
      };
      expect(timeArray([
        {timeEnd: "00:00"},
      ], _options)).to.be(message);
    });

    it("Formats error message correctly", function() {
      options.message.count = "foo %{count} bar";
      expect(timeArray([
        {timeStart: "00:00"},
      ], options)).to.be("foo " + options.count + " bar");
    });
  });

  describe("fieldContainer", function () {
    it("can be a field name", function() {
      const msg = "msg";
      const options = {
        fieldContainer: "field",
        field: "time",
        count: 2,
        message: {
          count: msg
        }
      };

      expect(timeArray({
        field: [
          {time: undefined},
          {time: undefined}
        ]
      }, options)).to.be(msg);

      expect(timeArray({
        field: [
          {time: "00:00"},
          {time: "00:00"}
        ]
      }, options)).to.be(undefined);
    });

    it("can be a JSON pointer", function() {
      const msg = "msg";
      const options = {
        field: "time",
        fieldContainer : "/nested/field",
        count: 2,
        message: {
          count: msg
        }
      };

      expect(timeArray({
        nested: {
          field: [
            {time: undefined},
            {time: undefined}
          ]
        }
      }, options)).to.be(msg);

      expect(timeArray({
        nested: {
          field: [
            {time: "00:00"},
            {time: "00:00"}
          ]
        }
      }, options)).to.be(undefined);
    });
  });

  describe("range", function() {
    const msg = "range %{lowerRange} %{upperRange}";
    const options = {
      lowerRange: "04:00",
      upperRange: "05:00",
      field: "timeStart",
      message: {
        range: msg
      }
    };

    it("doesn't allow above upperRange", function() {
      expect(timeArray([
        {timeStart: "04:30"},
        {timeStart: "06:00"},
      ], options)).to.be("range 04:00 05:00");
    });

    it("doesn't allow below lowerRange", function() {
      expect(timeArray([
        {timeStart: "04:30"},
        {timeStart: "03:00"},
      ], options)).to.be("range 04:00 05:00");
    });

    it("allows in range", function() {
      expect(timeArray([
        {timeStart: "04:30"},
        {timeStart: "04:31"},
        {timeStart: "04:31"},
      ], options)).to.be(undefined);
    });

    it("allows JSON pointer as lower and upper range", function() {
      const _options = {
        ...options,
        lowerRange: "/nested/lower",
        upperRange: "/nested/upper",
        fieldContainer: "/nested/field"
      };

      expect(timeArray({
        nested: {
          lower: "04:00",
          upper: "05:00",
          field: [
            {timeStart: "03:00"},
          ]
        }
      }, _options)).to.be("range 04:00 05:00");

      expect(timeArray({
        nested: {
          lower: "04:00",
          upper: "05:00",
          field: [
            {timeStart: "06:00"},
          ]
        }
      }, _options)).to.be("range 04:00 05:00");

      expect(timeArray({
        nested: {
          lower: "04:00",
          upper: "05:00",
          field: [
            {timeStart: "04:30"},
          ]
        }
      }, _options)).to.be(undefined);
    });
  });
});
