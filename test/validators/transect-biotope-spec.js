import {lajiValidate, expect} from "../bootstrap";

describe("validators.transectBiotope", function() {

  const {transectBiotope} = lajiValidate.validators;

  it("allows empty value", function() {
    expect(transectBiotope()).to.be(undefined);
    expect(transectBiotope("")).to.be(undefined);
  });

  it("allows valid biotope codes", function() {
    expect(transectBiotope("K3K2")).to.be(undefined);
    expect(transectBiotope("K3K35")).to.be(undefined);
    expect(transectBiotope("K5K12")).to.be(undefined);
    expect(transectBiotope("PUK2")).to.be(undefined);
    expect(transectBiotope("PSK1")).to.be(undefined);
    expect(transectBiotope("PSK7")).to.be(undefined);
    expect(transectBiotope("HAA1")).to.be(undefined);
    expect(transectBiotope("VAA6")).to.be(undefined);
    expect(transectBiotope("APK35")).to.be(undefined);
    expect(transectBiotope("X2")).to.be(undefined);
    expect(transectBiotope("AM")).to.be(undefined);
  });

  it("doesn't allow invalid biotope", function() {
    const msg = "msg";
    expect(transectBiotope("R5K12", {message: msg})).to.be(msg);
    expect(transectBiotope("GUK2", {message: msg})).to.be(msg);
    expect(transectBiotope("sAA1", {message: msg})).to.be(msg);
    expect(transectBiotope("DAA6", {message: msg})).to.be(msg);
    expect(transectBiotope("aPK35", {message: msg})).to.be(msg);
  });

  it("doesn't allow invalid focus", function() {
    const msg = "msg";
    expect(transectBiotope("A", {message: msg})).to.be(msg);
    expect(transectBiotope("K9K12", {message: msg})).to.be(msg);
    expect(transectBiotope("PRK2", {message: msg})).to.be(msg);
    expect(transectBiotope("PRK1", {message: msg})).to.be(msg);
    expect(transectBiotope("HaA1", {message: msg})).to.be(msg);
    expect(transectBiotope("VTA6", {message: msg})).to.be(msg);
  });

  it("doesn't allow invalid size", function() {
    const msg = "msg";
    expect(transectBiotope("K3A2", {message: msg})).to.be(msg);
    expect(transectBiotope("K3K0", {message: msg})).to.be(msg);
    expect(transectBiotope("K3K1", {message: msg})).to.be(msg);
    expect(transectBiotope("K5K36", {message: msg})).to.be(msg);
    expect(transectBiotope("P5K8", {message: msg})).to.be(msg);
    expect(transectBiotope("P5K0", {message: msg})).to.be(msg);
    expect(transectBiotope("PUA2", {message: msg})).to.be(msg);
    expect(transectBiotope("HAK2", {message: msg})).to.be(msg);
    expect(transectBiotope("VAA0", {message: msg})).to.be(msg);
    expect(transectBiotope("VAA7", {message: msg})).to.be(msg);
    expect(transectBiotope("X2K12", {message: msg})).to.be(msg);
  });

  it("doesn't allow other invalid values", function() {
    const msg = "msg";
    expect(transectBiotope("K1K20 jotain", {message: msg})).to.be(msg);
    expect(transectBiotope("X22", {message: msg})).to.be(msg);
  });
});
