import {lajiValidate, expect} from "../bootstrap";

describe("validators.monthDay", function() {

  const {monthDay} = lajiValidate.validators;


  it("it allows empty values", function() {
    expect(monthDay("")).to.be(undefined);
    expect(monthDay(undefined)).to.be(undefined);
    expect(monthDay({})).to.be(undefined);
  });

  it("allows valid months and dates with earliest", function() {
    const options = {earliestDay: 3, earliestMonth: 4};
    expect(monthDay("2013-10-01", options)).to.be(undefined);
    expect(monthDay("2978-04-03", options)).to.be(undefined);
  });

  it("doesn't allow dates that are too early", function() {
    const message = "msg";
    const options = {earliestDay: 3, earliestMonth: 4, message: message};
    expect(monthDay("2013-04-01", options)).to.be(message);
    expect(monthDay("2978-03-06", options)).to.be(message);
  });

  it("allows valid months and dates with latest", function() {
    const options = {latestDay: 3, latestMonth: 4};
    expect(monthDay("2013-04-03", options)).to.be(undefined);
    expect(monthDay("2978-01-01", options)).to.be(undefined);
  });

  it("doesn't allow dates that are too late", function() {
    const message = "msg";
    const options = {latestDay: 3, latestMonth: 4, message: message};
    expect(monthDay("2013-04-22", options)).to.be(message);
    expect(monthDay("2978-08-01", options)).to.be(message);
  });
});
