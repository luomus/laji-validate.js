import {lajiValidate, expect} from "../bootstrap";

describe("validators.unitCount", function() {

  const {unitCount} = lajiValidate.validators;

  it("allows empty value", function() {
    expect(unitCount()).to.be(undefined);
    expect(unitCount("")).to.be(undefined);
  });

  it("allows valid cases", function() {
    expect(unitCount({units: []}, {max: 1})).to.be(undefined);
    expect(unitCount({units: []}, {min: 0})).to.be(undefined);
    expect(unitCount({units: [{count: 1}]}, {min: 1})).to.be(undefined);
    expect(unitCount({units: [{count: 1}]}, {max: 1})).to.be(undefined);
    expect(unitCount({units: [{count: 1}, {count: 1}]}, {min: 1})).to.be(undefined);
    expect(unitCount({units: [{count: 1}, {count: 1}]}, {max: 2})).to.be(undefined);
    expect(unitCount({units: [{count: 1, juvenileIndividualCount: 1}, {count: 1}]}, {max: 2})).to.be(undefined);
  });

  it("does not allow invalid cases", function() {
    const msg = "msg";
    expect(unitCount({units: []}, {max: -1, message: msg})).to.be(msg);
    expect(unitCount({units: []}, {min: 1, message: msg})).to.be(msg);
    expect(unitCount({units: [{count: 1}]}, {min: 2, message: msg})).to.be(msg);
    expect(unitCount({units: [{count: 1}]}, {max: 0, message: msg})).to.be(msg);
    expect(unitCount({units: [{count: 1}, {count: 1}]}, {min: 3, message: msg})).to.be(msg);
    expect(unitCount({units: [{count: 1}, {count: 1}]}, {max: 1, message: msg})).to.be(msg);
  });

  it("allows conditional validation based on the field value", function() {
    const msg = "msg";
    const doc = {foo: false, units: [{pairCount: 1}]};
    expect(unitCount(false, {max: 0, onlyWhenValue: true, message: msg}, "foo", doc)).to.be(undefined);
    expect(unitCount(false, {max: 0, onlyWhenValue: false, message: msg}, "foo", doc)).to.be(msg);
  });

  it("allows conditional validation based on another property", function() {
    const msg = "msg";
    expect(unitCount({foo: "bar", units: [{count: 1}]}, {max: 0, onlyWhenField: "foo", onlyWhenFieldEquals: "bar",  message: msg})).to.be(msg);
    expect(unitCount({foo: "bar", units: [{count: 1}]}, {max: 0, onlyWhenField: "foo", onlyWhenFieldEquals: "not",  message: msg})).to.be(undefined);
    expect(unitCount({foo: "bar", units: [{count: 1}]}, {max: 0, onlyWhenField: "not", onlyWhenFieldEquals: "bar",  message: msg})).to.be(undefined);
    expect(unitCount({foo: {field: "bar"}, units: [{count: 1}]}, {max: 0, onlyWhenField: "foo/field", message: msg})).to.be(msg);
    expect(unitCount({foo: {field: "bar"}, units: [{count: 1}]}, {max: 0, onlyWhenField: "foo/field", onlyWhenFieldEquals: "bar", message: msg})).to.be(msg);
    expect(unitCount({foo: {field: "bar"}, units: [{count: 1}]}, {max: 3, onlyWhenField: "foo/field", onlyWhenFieldEquals: "bar2", message: msg})).to.be(undefined);
  });

  it("can select gathering level as it's root", function() {
    const doc = {
      gatherings: [
        {
          gatheringFacts: {
            biotopeCounted: true
          },
          units: [
            {count: 1}
          ]
        },
        {
          gatheringFacts: {
            biotopeCounted: true
          },
          units: [
            {count: 1},
            {count: 1}
          ]
        }
      ]
    };
    const testObj1 = doc.gatherings[0].gatheringFacts;
    const testObj2 = doc.gatherings[1].gatheringFacts;
    const msg = "error";
    expect(unitCount(true, {min: 2, message: msg, unitsField: "../units"}, "biotopeCounted", testObj1, {}, doc)).to.be(msg);
    expect(unitCount(true, {min: 2, message: msg, unitsField: "../units"}, "biotopeCounted", testObj2, {}, doc)).to.be(undefined);
  });

});
