import {lajiValidate, expect} from "../bootstrap";

describe("validators.waterBirdPairCount", function() {

  const {waterBirdShortHand: waterBirdShortHand} = lajiValidate.validators;

  it("allows empty value", function() {
    expect(waterBirdShortHand()).to.be(undefined);
    expect(waterBirdShortHand("")).to.be(undefined);
  });

  it("allows valid short hand", function() {
    expect(waterBirdShortHand("2k")).to.be(undefined);
    expect(waterBirdShortHand("2k, n, Ä, 3")).to.be(undefined);
    expect(waterBirdShortHand("ä, 2, 10000")).to.be(undefined);
    expect(waterBirdShortHand("2kn")).to.be(undefined);
    expect(waterBirdShortHand("2knÄ, 3, k10Ä17ä")).to.be(undefined);
  });

  it("doesn't allow invalid short hand", function() {
    const msg = "msg";
    expect(waterBirdShortHand("3kj", {message: msg})).to.be(msg);
    expect(waterBirdShortHand("kjn", {message: msg})).to.be(msg);
    expect(waterBirdShortHand(":D", {message: msg})).to.be(msg);
  });
});
