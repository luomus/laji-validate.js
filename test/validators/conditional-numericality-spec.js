import {lajiValidate, expect} from "../bootstrap";

describe("validators.conditionalCompareNumerically", function() {

  const {conditionalNumericality: conditionalNumericality} = lajiValidate.validators;
  const context = {
    val1: 1,
    val2: "ab",
    val3: "bar",
  };
  context.nested = context;

  it("allows empty value", function() {
    expect(conditionalNumericality(undefined, {}, "foo", context)).to.be(undefined);
    expect(conditionalNumericality("", {}, "foo", context)).to.be(undefined);
  });

  it("allows valid inputs", function() {
    expect(conditionalNumericality("-3", {
      field: "val1",
      lessThan: {1: -2, 2: -5, 3: 10}
    }, "foo", context)).to.be(undefined);
    expect(conditionalNumericality(2, {
      field: "val2",
      greaterThan: {"a": 2, "ab": 1, "abc": 4}
    }, "foo", context)).to.be(undefined);
  });

  it("doesn't allow invalid inputs", function() {
    const msg = "msg";
    expect(conditionalNumericality("1", {
      field: "val1",
      lessThan: {1: -2, 2: -5, 3: 10},
      message: msg
    }, "foo", context)).to.be(msg);
    expect(conditionalNumericality(10, {
      field: "val3",
      greaterThan: {"bar": 10, "bor": 9},
      message: msg
    }, "foo", context)).to.be(msg);
  });
});
