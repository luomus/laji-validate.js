require("babel-register");

var lajiValidate = require("../src/main");
var expect = require("expect.js");

module.exports = {
  lajiValidate,
  expect
};
