const moment = require("moment");

var validate = module.exports = require("@luomus/validate");

// Set default options
validate.options = {
  fullMessages: false,
  format: "jsonPath"
};

validate.async.options = {
  cleanAttributes: false,
  fullMessages: false,
  format: "jsonPath"
};

const ISO_8601_DATE = "YYYY-MM-DD";
const ISO_8601_DATETIME = moment.ISO_8601;

validate.validators.remote = require("./validators/remote");
validate.validators.time = require("./validators/time");
validate.validators.geometry = require("./validators/geometry");
validate.validators.monthDay = require("./validators/month-day");
validate.validators.monthDayRange = require("./validators/month-day-range");
validate.validators.crossCheck = require("./validators/cross-check");
validate.validators.compareDate = require("./validators/compare-date");
validate.validators.transectBiotope = require("./validators/transect-biotope");
validate.validators.compareNumerically = require("./validators/compare-numerically");
validate.validators.duplicateCheck = require("./validators/duplicate-check");
validate.validators.stringArray = require("./validators/string-array");
validate.validators.timeArray = require("./validators/time-array");
validate.validators.unitCount = require("./validators/unit-count");
validate.validators.timeDifference = require("./validators/time-difference");
validate.validators.valueInArray = require("./validators/value-in-array");
validate.validators.conditionalNumericality = require("./validators/conditional-numericality");
validate.validators.waterBirdShortHand = require("./validators/water-bird-short-hand");
validate.validators.waterBirdPairCount = require("./validators/water-bird-pair-count");
validate.validators.arrayDuplicateCheck = require("./validators/array-duplicate-check");

// This can be moved when no longer needed
validate.validators.dateSpan = (value, options) => {
  if (typeof options === "undefined") {
    options = {};
  }
  const message = options.message || "date begin (%{dateBegin}) must be before date end (%{dateEnd})";
  const format = options.dateOnly ? ISO_8601_DATE : ISO_8601_DATETIME;
  if (value) {
    const {dateBegin, dateEnd} = value;
    if ((dateBegin && dateEnd && moment(dateBegin, format).isAfter(moment(dateEnd, format))) ||
        (dateEnd && !dateBegin)) {
      let errorMessage = message || "";
      ["dateBegin", "dateEnd"].forEach(field => {
        while (value[field] && errorMessage.includes(`%{${field}}`)) errorMessage = errorMessage.replace(`%{${field}}`, value[field]);
      });
      return errorMessage;
    }
  }
};

validate.extend(validate.validators.datetime, {
  parse: (value, options) => {
    if (value === "now") {
      const lastMonth = moment().add(12, "hours");
      return +moment.utc(lastMonth.format("YYYY-MM-DD HH:mm:ss"));
    } else if (value === "lastMonth") {
      const lastMonth = moment().subtract(1, "months");
      const format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD HH:mm:ss";
      return +moment.utc(lastMonth.format(format), format, true);
    } else if (value === "lastSixMonths") {
      const lastSixMonths = moment().subtract(6, "months");
      const format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD HH:mm:ss";
      return +moment.utc(lastSixMonths.format(format), format, true);
    } else if (value === "lastYear") {
      const lastMonth = moment().subtract(1, "years");
      const format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD HH:mm:ss";
      return +moment.utc(lastMonth.format(format), format, true);
    } else if (value === "nowExact") {
      // need to fake that the current time is utc so that date validators would work
      return +moment().utc(moment().format("YYYY-MM-DD HH:mm:ss"));
    }
    const format = options.dateOnly ? ISO_8601_DATE : ISO_8601_DATETIME;
    return +moment.utc(value, format, true);
  },
  format: (value, options) => {
    const format = options.dateOnly ? "YYYY-MM-DD" : "YYYY-MM-DD HH:mm:ss";
    return moment(value).format(format);
  }
});

const runValidations = validate.runValidations;
validate.runValidations = (attributes, constraints, options, formData) => {
  if (typeof attributes === "object" && !Array.isArray(attributes) && attributes !== null && attributes.skipped) {
    return [];
  }
  return runValidations(attributes, constraints, options, formData);
};
