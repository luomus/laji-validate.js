const util = require("../util");
const waterBirdShortHand = require("./water-bird-short-hand");

const defaultOptions = {
  shortHandField: "shortHandText",
  message: {
    tooBig: "Pair count is too big"
  }
};

function getTotalCount(shortHandText) {
  shortHandText = shortHandText.replace(/\s+/g, ""); // remove spaces
  shortHandText = shortHandText
    .replace(/K/g, "k")
    .replace(/N/g, "n")
    .replace(/P/g, "p")
    .replace(/\./g, ",");
  shortHandText = shortHandText
    .replace(/p+/g, "p")
    .replace(/k+/g, "k")
    .replace(/n+/g, "n")
    .replace(/Ä+/g, "Ä")
    .replace(/ä+/g, "ä"); // replace repeated characters with one

  let totalCount = 0;

  shortHandText.split(",").filter(val => !!val).map(val => {
    const observations = val.match(/(?!$)\d*([knpÄä]|$)/g);
    observations.forEach(obs => {
      const parts = obs.split(/(?=[knpÄä])/g);
      const count = !isNaN(parseInt(parts[0], 10)) ? parseInt(parts[0], 10) : 1;
      const type = isNaN(parseInt(parts[parts.length - 1], 10)) ? parts[parts.length - 1] : undefined;

      if (type === "p") {
        totalCount += 2 * count;
      } else {
        totalCount += count;
      }
    });
  });

  return totalCount;
}

module.exports = function waterBirdPairCount(value, options, key, attributes, globalOptions, formData) {
  options = util.extend({}, defaultOptions, options);

  if (typeof value !== "number") {
    return;
  }

  const shortHandText = util.parseJSONPointer(options.shortHandField, attributes, formData);

  if (!shortHandText || waterBirdShortHand(shortHandText, {}) !== undefined) {
    return;
  }

  if (value > getTotalCount(shortHandText)) {
    return util.errorMessage("tooBig", options, value);
  }
};
