const moment = require("moment");
const util = require("../util");

const defaultOptions = {
  minDifference: undefined,
  maxDifference: undefined,
  compareTo: undefined,
  message: {
    tooSmall: "time difference is less than %{limit} minutes",
    tooBig: "time difference is more than %{limit} minutes"
  }
};

function getMessage(errorKey, options, key, limit) {
  let msg = "%{key} is invalid";
  if (options && options.message) {
    msg = typeof options.message === "string" ? options.message : (options.message[errorKey] || msg);
  }
  return msg.replace("%{key}", key).replace("%{limit}", limit);
}

module.exports = function timeDifference(value, options, key, attributes, globalOptions, formData) {
  options = util.extend({}, defaultOptions, options);

  // cannot do anything with value that is not string
  if (typeof value !== "string" || !value) {
    return;
  }
  const value2 = util.parseJSONPointer(options.compareTo, attributes, formData);
  if (typeof value2 !== "string" || !value2) {
    return;
  }

  const parts = value.split(":");
  const parts2 = value2.split(":");

  const now = new Date();
  const time = moment(now);
  time.set({hour: parseInt(parts[0], 10), minute: parseInt(parts[1], 10)});
  const time2 = moment(now);
  time2.set({hour: parseInt(parts2[0], 10), minute: parseInt(parts2[1], 10)});

  const duration = moment.duration(time.diff(time2));
  const difference = duration.asMinutes();

  if (options.minDifference && difference < options.minDifference) {
    return getMessage("tooSmall", options, key, options.minDifference);
  }
  if (options.maxDifference && difference > options.maxDifference) {
    return getMessage("tooBig", options, key, options.maxDifference);
  }
};
