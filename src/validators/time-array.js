const util = require("../util");
const moment = require("moment");

const defaultOptions = {
  field: "timeStart",
  order: false,
  count: undefined,
  upperRange: undefined,
  lowerRange: undefined,
  message: {
    order: "Times must be chronologically in order",
    count: "More than %{count} times must be given",
    range: "Times must be between %{lowerRange}-%{upperRange}",
  }
};

function getMessage(errorKey, options, key) {
  let msg = "%{key} is required when %{target} is given";
  if (options && options.message) {
    msg = typeof options.message === "string" ? options.message : (options.message[errorKey] || msg);
  }
  return msg
    .replace("%{key}", key)
    .replace("%{count}", options.count)
    .replace("%{upperRange}", options.upperRange)
    .replace("%{lowerRange}", options.lowerRange);
}

module.exports = function timeArray(value, options, key, attributes) {
  options = util.extend({}, defaultOptions, options);
  const {field, fieldContainer, order, count} = options;
  let {upperRange, lowerRange} = options;

  const origValue = value;
  value = !fieldContainer
    ? value
    : util.isJSONPointer(fieldContainer)
      ? util.parseJSONPointer(fieldContainer, value, attributes)
      : value[fieldContainer];

  // cannot do anything with value that is not an array
  if (!Array.isArray(value)) {
    return;
  }

  if (order) {
    let prevTime = undefined;
    if (!value.every((item = {}) => {
      const time = item[field];
      const timeMoment = moment(time, "HH:mm");
      const prevTimeMoment = moment(prevTime, "HH:mm");
      let isOk = (!time || !prevTime) || timeMoment.isAfter(prevTimeMoment) || timeMoment.isSame(prevTimeMoment);
      if (time) {
        prevTime = time;
      }
      return isOk;
    })) {
      return getMessage("order", options, key);
    }
  }

  if (count !== undefined) {
    if (value.filter((item = {}) => {
      return item[field];
    }).length < count) {
      return getMessage("count", options, key);
    }
  }

  if (upperRange || lowerRange) {
    if (util.isJSONPointer(upperRange)) {
      upperRange = util.parseJSONPointer(upperRange, origValue, attributes);
    }
    if (util.isJSONPointer(lowerRange)) {
      lowerRange = util.parseJSONPointer(lowerRange, origValue, attributes);
    }
    const upperMoment = upperRange && moment(upperRange, "HH:mm");
    const lowerMoment = lowerRange && moment(lowerRange, "HH:mm");
    if (value.some((item = {}) => {
      const time = moment(item[field], "HH:mm");
      return upperMoment && time.isAfter(upperMoment) || lowerMoment && time.isBefore(lowerMoment);
    })) {
      return getMessage("range", {...options, upperRange, lowerRange}, key);
    }
  }
};
