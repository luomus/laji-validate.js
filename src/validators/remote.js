const util = require("../util");

const defaultOptions = {
  validator: undefined,
  path: undefined,
  message: {
    invalidValue: "invalid value given.",
    genericError: "something bad happened..."
  }
};

function getMessage(errorKey, options, key, target) {
  let msg = "%{key} is required when %{target} is given";
  if (options && options.message) {
    msg = typeof options.message === "string" ? options.message : (options.message[errorKey] || msg);
  }
  return msg.replace("%{key}", key).replace("%{target}", target);
}

module.exports = function remote(value, options, key, attributes, globalOptions, formData) {
  if (!value) {
    return;
  }
  const hasUserMessage = options && options.message;
  options = util.extend({}, defaultOptions, options);
  const path = options.path || (key.startsWith(".") ? key : "." + key);
  return new Promise((resolve) => {
    this.fetch(
      "/documents/validate",
      {
        validator: options.validator,
        field: path,
        informalTaxonGroup: options.informalTaxonGroup,
        validationErrorFormat: "jsonPath"
      },
      {
        method: "POST",
        headers: {
          "accept": "application/json",
          "content-type": "application/json"
        },
        body: JSON.stringify(formData)
      })
      .catch(function (err) {
        if (typeof err._body === "string") {
          return {
            status: 422,
            json: () => JSON.parse(err._body)
          };
        }
        return { status: 0 };
      })
      .then(response => {
        if (!response.status) {
          return response;
        }
        if (response.status >= 200 && response.status < 300) {
          return resolve();
        }
        if (response.status >= 400 && response.status < 500) {
          return response.json();
        }
        return resolve(getMessage("genericError", options, key));
      })
      .then((body) => {
        if (body && body.error && body.error.details && typeof body.error.details === "object") {
          const key = Object.keys(body.error.details)[0];
          if (hasUserMessage || !key) {
            return resolve(getMessage("invalidValue", options, key));
          }
          if (body.error.details[path]) {
            return resolve(body.error.details[path].join(", "));
          }
          if (body.error.details[key]) {
            return resolve(body.error.details[key].join(", "));
          }
          return resolve(getMessage("invalidValue", options, key));
        }
        return resolve();
      });
  });

};
