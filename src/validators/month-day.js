const util = require("../util");
const moment = require("moment");

const defaultOptions = {
  earliestDay: undefined,
  latestDay: undefined,
  earliestMonth: undefined,
  latestMonth: undefined,
  message: {
    tooEarly: "cannot be earlier than %{value}",
    tooLate: "cannot be later than %{value}"
  }
};

function getMessage(errorKey, options, key, value) {
  let msg = "%{key} is invalid biotope code";
  if (options && options.message) {
    msg = typeof options.message === "string" ? options.message : (options.message[errorKey] || msg);
  }
  return msg.replace("%{key}", key).replace("%{value}", value);
}

function getDate(options, fields) {
  if (options[fields[0]] && options[fields[1]]) {
    return options[fields[0]] + "." + options[fields[1]] + ".";
  }
  return options[fields[0]] + options[fields[1]];
}

function getParsedData(value) {
  const date = moment.utc(value);
  return [+date.format("MM"), +date.format("DD")];
}

module.exports = function monthDay(value, options, key) {
  options = util.extend({}, defaultOptions, options);

  // cannot do anything with value that is not string
  if (typeof value !== "string") {
    return;
  }
  const data = getParsedData(value);
  if (options.earliestMonth) {
    if (data[0] < options.earliestMonth) {
      return getMessage("tooEarly", options, key, getDate(options, ["earliestDay", "earliestMonth"]));
    }
    if (data[0] === options.earliestMonth && options.earliestDay && data[1] < options.earliestDay) {
      return getMessage("tooEarly", options, key, getDate(options, ["earliestDay", "earliestMonth"]));
    }
  } else if (options.earliestDay && data[1] < options.earliestDay) {
    return getMessage("tooEarly", options, key, getDate(options, ["earliestDay", "earliestMonth"]));
  }

  if (options.latestMonth) {
    if (data[0] > options.latestMonth) {
      return getMessage("tooLate", options, key, getDate(options, ["latestDay", "latestMonth"]));
    }
    if (data[0] === options.latestMonth && options.latestDay && data[1] > options.latestDay) {
      return getMessage("tooLate", options, key, getDate(options, ["latestDay", "latestMonth"]));
    }
  } else if (options.latestDay && data[1] > options.latestDay) {
    return getMessage("tooLate", options, key, getDate(options, ["latestDay", "latestMonth"]));
  }
};
