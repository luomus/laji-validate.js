const util = require("../util");

const defaultOptions = {
  greaterThan: undefined,
  greaterThanOrEqualTo: undefined,
  lessThan: undefined,
  lessThanOrEqualTo: undefined,
  equalTo: undefined,
  onlyWhenAttrIn: undefined,
  message: {
    tooSmall: "%{key} is too small compared to %{field}",
    tooBig:   "%{key} is too small compared to %{field}",
  }
};

function getMessage(errorKey, options, key, field) {
  let msg = "%{key} is invalid compared to %{field}";
  if (options && options.message) {
    msg = typeof options.message === "string" ? options.message : (options.message[errorKey] || msg);
  }
  return msg.replace("%{key}", key).replace("%{field}", field);
}

function getNumericValue(attr, key) {
  if (Array.isArray(key)) {
    let sum = 0;
    for (let i = 0; i < key.length; i++) {
      let value = typeof key[i] !== "number" ? util.parseJSONPointer(key[i], attr) : key[i];
      if (value) {
        value = getNumericValueForOne(attr, key[i]);
        if (value === null) { return null; }
        sum += value;
      }
    }
    return sum;
  } else {
    return getNumericValueForOne(attr, key);
  }
}

function getNumericValueForOne(attr, key) {
  if (typeof key === "number") {
    return key;
  }
  const value = typeof attr === "object" ? util.parseJSONPointer(key, attr) : attr;
  if (!value) {
    return null;
  }
  if (typeof value === "number") {
    return value;
  }
  const num = ("" + value).match(/[\d-\.]/g);
  if (!Array.isArray(num)) {
    return null;
  }
  return +(num.join(""));
}

module.exports = function compareNumerically(value, options, key, attributes) {
  // check that the context is an object
  if (typeof attributes !== "object") {
    return;
  }

  // Only do work when value is string or number
  if (typeof value !== "string" && typeof value !== "number") {
    return;
  }

  value = getNumericValue(value);
  options = util.extend({}, defaultOptions, options);

  if (value === null) {
    return;
  }

  if (options.onlyWhenAttrIn) {
    for (let key in options.onlyWhenAttrIn) {
      const parsedAttr = util.parseJSONPointer(key, attributes);
      if (!parsedAttr || options.onlyWhenAttrIn[key].indexOf(parsedAttr) === -1) {
        return;
      }
    }
  }

  if (options.greaterThan) {
    const number = getNumericValue(attributes, options.greaterThan);
    if (number !== null && value <= number) {
      return getMessage("tooSmall", options, key, options.greaterThan);
    }
  }
  if (options.greaterThanOrEqualTo) {
    const number = getNumericValue(attributes, options.greaterThanOrEqualTo);
    if (number !== null && value < number) {
      return getMessage("tooSmall", options, key, options.greaterThanOrEqualTo);
    }
  }

  if (options.lessThan) {
    const number = getNumericValue(attributes, options.lessThan);
    if (number !== null && value >= number) {
      return getMessage("tooBig", options, key, options.lessThan);
    }
  }
  if (options.lessThanOrEqualTo) {
    const number = getNumericValue(attributes, options.lessThanOrEqualTo);
    if (number !== null && value > number) {
      return getMessage("tooSmall", options, key, options.lessThanOrEqualTo);
    }
  }

  if (options.equalTo) {
    const number = getNumericValue(attributes, options.equalTo);
    if (number !== null && value !== number) {
      if (value < number) {
        return getMessage("tooSmall", options, key, options.equalTo);
      } else {
        return getMessage("tooBig", options, key, options.equalTo);
      }
    }
  }
};
