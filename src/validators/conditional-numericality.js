const util = require("../util");
const compareNumerically = require("./compare-numerically");

const defaultOptions = {
  field: "",
  greaterThan: {},
  greaterThanOrEqualTo: {},
  lessThan: {},
  lessThanOrEqualTo: {},
  equalTo: {},
  message: {
    tooSmall: "%{key} is too small compared to %{field}",
    tooBig:   "%{key} is too big compared to %{field}",
  }
};

module.exports = function conditionalNumericality(value, options, key, attributes, globalOptions, formData) {
  options = util.extend({}, defaultOptions, options);
  const newOptions = {message: options.message};

  const fieldValue = util.parseJSONPointer(options.field, attributes, formData);
  if (fieldValue === undefined) {
    return;
  }

  for (const comparisonType of ["greaterThan", "greaterThanOrEqualTo", "lessThan", "lessThanOrEqualTo", "equalTo"]) {
    if (options[comparisonType] && options[comparisonType][fieldValue] !== undefined) {
      newOptions[comparisonType] = options[comparisonType][fieldValue];
    }
  }

  return compareNumerically(value, newOptions, key, attributes, globalOptions, formData);
};
