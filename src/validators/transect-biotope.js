const util = require("../util");

const defaultOptions = {
  message: {
    notString: "Value in %{key} is not of type string",
    invalidBiotope: "Biotope should be one of K,M,S,L,J,P,H,R,N,V,A,T or X but \"%{value}\" given",
    invalidFocus: "Invalid second character should be one of %{key}, but got %{value}",
    invalidSize: "Invalid measurement given should be %{key}, but got %{value}",
    invalidK: "When K is given, number should be between 1-7. Now \"%{value}\" given.",
    invalidPK: "When K is given for trees, number should be between 2-35. Now \"%{value}\" given.",
    invalidA: "When A is given, number should be between 1-6. Now \"%{value}\" given.",
    invalidX: "Invalid biotope other. Should only contain focusing character after X and nothing else",
    tooLong: "Biotope code has some extra values that shouldn\"t be there. Place use notes fields instead",
    tooShort: "Biotope code is too sort!"
  }
};

function getMessage(errorKey, options, key, value) {
  let msg = "%{key} is invalid biotope code";
  if (options && options.message) {
    msg = typeof options.message === "string" ? options.message : (options.message[errorKey] || msg);
  }
  return msg.replace("%{key}", key).replace("%{value}", value);
}

function checkCodes(options, value, allowedSecond, size) {
  if (value.length < (size? 3 : 2)) {
    return getMessage("tooShort", options, undefined, value);
  }
  const secondChar = value.charAt(1);
  if (allowedSecond.indexOf(secondChar) === -1) {
    return getMessage("invalidFocus", options, allowedSecond.join(","), secondChar);
  }
  if (!size) {
    if (value.length > 2) {
      return getMessage("invalidX", options);
    }
    return;
  }
  const firstChar = value.charAt(0);
  const thirdChar = value.charAt(2);
  if (thirdChar !== size) {
    return getMessage("invalidSize", options, size, thirdChar);
  }
  const rest = value.substr(3);
  const area = parseInt(rest);
  const minLen = firstChar === "P" ? 1 : 2;
  const maxLen = firstChar === "P" ? 7 : 35;
  if (rest.length !== ("" + area).length) {
    return getMessage("tooLong", options, size, thirdChar);
  }
  if ((size === "K" && area >= minLen && area <= maxLen) ||
      (size === "A" && area >= 1 && area <= 6)) {
    return;
  }
  return getMessage("invalid" + (firstChar === "P" ? "P" : "") + size, options, undefined, rest);
}

module.exports = function transectBiotope(value, options, key) {
  options = util.extend({}, defaultOptions, options);

  // allow empty
  if (value === "" || typeof value === "undefined") {
    return;
  }

  // check that the value is an string
  if (typeof value !== "string") {
    return getMessage("notString", options, key, value);
  }

  const firstChar = value.charAt(0);
  if (["K", "M", "S", "L", "J", "P", "H", "R", "N", "V", "A", "T", "X"].indexOf(firstChar) === -1) {
    return getMessage("invalidBiotope", options, key, firstChar);
  }

  switch (firstChar) {
  case "K":
  case "M":
  case "S":
  case "L":
  case "J":
    return checkCodes(options, value, ["1","2","3","4","5","6"], "K");
  case "P":
    return checkCodes(options, value, ["H","S","L","U"], "K");
  case "H":
    return checkCodes(options, value, ["H","A","S"], "A");
  case "R":
    return checkCodes(options, value, ["R","O","L"], "K");
  case "N":
    return checkCodes(options, value, ["N","M","K","T","R"], "A");
  case "V":
    return checkCodes(options, value, ["A","S","H","N","R"], "A");
  case "A":
    return checkCodes(options, value, ["M","V","T","K","P"], value.length > 2 ? "K" : undefined);
  case "T":
    return checkCodes(options, value, ["N","S","K","A"], "A");
  case "X":
    return checkCodes(options, value, ["1","2","3"]);
  }
};
