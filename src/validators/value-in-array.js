const util = require("../util");

const defaultOptions = {
  value: undefined,
  arrayField: "",
  arrayFieldKey: "",
  arrayValueMap: {},
  onlyWhenField: "",
  onlyWhenFieldIn: false,
  conditions: false, // array of conditions
  message: {
    missing: "%{key} does not belong to %{target}",
  }
};

function getMessage(errorKey, options, key, target) {
  let msg = "%{key} is invalid";
  if (options && options.message) {
    msg = typeof options.message === "string" ? options.message : (options.message[errorKey] || msg);
  }
  return msg.replace("%{key}", key).replace("%{target}", target);
}

module.exports = function valueInArray(value, options, key, attributes, globalOptions, formData) {
  options = util.extend({}, defaultOptions, options);

  // check that the value is an object
  if (typeof attributes !== "object") {
    return;
  }

  value = options.value != null ? options.value : value;

  if (options.conditions || options.onlyWhenField) {
    const conditions = options.conditions ? options.conditions : [options];

    for (const condition of conditions) {
      let searchValues = util.getCheckValues(condition.onlyWhenField, attributes, formData);
      if (searchValues.length < 1) {
        return;
      }
      if (condition.onlyWhenFieldIn && condition.onlyWhenFieldIn.every(v => searchValues.indexOf(v) === -1)) {
        return;
      }
    }
  }

  // pick the check value
  let checkValues = util.getCheckValues(options.arrayField, attributes, formData);
  checkValues = checkValues.reduce((result, value) => {
    if (options.arrayFieldKey) {
      value = util.parseJSONPointer(options.arrayFieldKey, value);
    }
    value = options.arrayValueMap[value] || value;
    if (Array.isArray(value)) {
      result = result.concat(value);
    } else {
      result.push(value);
    }
    return result;
  }, []);

  if (((typeof value === "string" || Array.isArray(value)) && value.length > 0) ||
    (typeof value === "number" && !isNaN(value)) || (typeof value === "boolean")) {
    if (checkValues.indexOf(value) === -1) {
      return getMessage("missing", options, key, options.arrayField);
    }
  }
};
