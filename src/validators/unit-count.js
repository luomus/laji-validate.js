const util = require("../util");

const defaultOptions = {
  onlyWhenValue: "", // Validated only when value is equal to this
  onlyWhenField: "", // Validated only if this field exists
  onlyWhenFieldEquals: "", // Validated only if the fields above exists and has this value
  countWhen: "hasCount", // Not used atm
  unitsField: null, // Path from the validated fields to units (can be relative)
  unitCountFields: ["count", "pairCount", "adultIndividualCount", "femaleIndividualCount", "individualCount", "juvenileIndividualCount", "maleIndividualCount", "abundanceString"], // count unit if any of these fields have value
  max: null, // Unit count needs to be smaller or equal than this
  min: null, // Unit count needs to be greater or equal than this
  message: {
    tooMany: "There should not be more than %{key} units. But now there was %{value} units",
    tooFew: "There should not be less than %{key} units. But now there was only %{value} units"
  }
};

module.exports = function unitCount(value, options, key, attributes, globalOptions, formData) {
  options = util.extend({}, defaultOptions, options);

  if (options.onlyWhenField) {
    let searchValue = util.parseJSONPointer(options.onlyWhenField, value, formData);
    if (searchValue === undefined || (options.onlyWhenFieldEquals !== "" && searchValue !== options.onlyWhenFieldEquals)) {
      return;
    }
  }

  if (options.onlyWhenValue !== "" && options.onlyWhenValue !== value) {
    return;
  }

  const units = options.unitsField ?
    util.parseJSONPointer(options.unitsField, attributes, formData) :
    (value && value.units ? value.units : (attributes && attributes.units ? attributes.units : undefined));
  let unitCnt = 0;
  if (typeof units !== "undefined") {
    for (const unit of units) {
      // TODO: add different counting methods when needed
      for (const cntField of options.unitCountFields) {
        if (typeof unit[cntField] !== "undefined" && unit[cntField] !== "" && unit[cntField] !== null) {
          unitCnt++;
          break;
        }
      }
    }
  }

  if (typeof options.max === "number" && options.max < unitCnt) {
    return util.errorMessage("tooMany", options, options.max, unitCnt);
  }

  if (typeof options.min === "number" && options.min > unitCnt) {
    return util.errorMessage("tooFew", options, options.min, unitCnt);
  }
};
