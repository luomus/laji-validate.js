const util = require("../util");

const defaultOptions = {
  requirePresence: false,
  earliestMinute: undefined,
  latestMinute: undefined,
  earliestHour: undefined,
  latestHour: undefined,
  message: {
    presence: "time is required for field %{key}",
    tooEarly: "cannot be earlier than %{limit}",
    tooLate: "cannot be later than %{limit}"
  }
};

function getMessage(errorKey, options, key, limit) {
  let msg = "%{key} is invalid biotope code";
  if (options && options.message) {
    msg = typeof options.message === "string" ? options.message : (options.message[errorKey] || msg);
  }
  return msg.replace("%{key}", key).replace("%{limit}", limit);
}

function getDate(options, fields) {
  if (options[fields[0]] && options[fields[1]]) {
    return "" + options[fields[0]] + "." + options[fields[1]];
  }
  return ("" + (options[fields[0]] || "")) + ("" + (options[fields[1]] || ""));
}

function getParsedTime(value) {
  const time = value.split(" ").pop().split("T").pop();
  const part = time.split(":");
  return [+(part[0]), +(part[1] || 0)];
}

module.exports = function time(value, options, key) {
  // cannot do anything with value that is not string
  if (typeof value !== "string") {
    return;
  }
  options = util.extend({}, defaultOptions, options);

  const data = getParsedTime(value);

  if (options.requirePresence && isNaN(data[0])) {
    return getMessage("presence", options, key);
  }

  if (options.earliestHour !== undefined) {
    if (data[0] < options.earliestHour) {
      return getMessage("tooEarly", options, key, getDate(options, ["earliestHour", "earliestMinute"]));
    }
    if (data[0] === options.earliestHour && options.earliestMinute != undefined && data[1] < options.earliestMinute) {
      return getMessage("tooEarly", options, key, getDate(options, ["earliestHour", "earliestMinute"]));
    }
  } else if (options.earliestMinute !== undefined && data[1] < options.earliestMinute) {
    return getMessage("tooEarly", options, key, getDate(options, ["earliestHour", "earliestMinute"]));
  }

  if (options.latestHour !== undefined) {
    if (data[0] > options.latestHour) {
      return getMessage("tooLate", options, key, getDate(options, ["latestHour", "latestMinute"]));
    }
    if (data[0] === options.latestHour && options.latestMinute !== undefined && data[1] > options.latestMinute) {
      return getMessage("tooLate", options, key, getDate(options, ["latestHour", "latestMinute"]));
    }
  } else if (options.latestMinute !== undefined && data[1] > options.latestMinute) {
    return getMessage("tooLate", options, key, getDate(options, ["latestHour", "latestMinute"]));
  }
};
