const util = require("../util");
const geojsonArea = require("@mapbox/geojson-area");
const geojsonBounds = require("geojson-bounds");
const { latLngTuplesDistance, convertGeoJSON } = require("@luomus/laji-map/lib/utils");

const defaultOptions = {
  requireShape: false,
  includeGatheringUnits: false,
  allowedTypes: [
    "Point",
    "MultiPoint",
    "LineString",
    "MultiLineString",
    "Polygon",
    "MultiPolygon",
    "GeometryCollection"
  ],
  allowedKeys: [
    "type",
    "coordinates",
    "coordinateVerbatim",
    "geometries",
  ],
  overlapWith: false,
  minDistanceWith: false,
  minDistance: 0,
  message: {
    notGeometry: "Value in %{key} is not correct geometry",
    missingType: "Value in %{key} is missing required type property",
    invalidRadius: "Radius should be number in %{key}",
    missingGeometries: "Geometries are required in %{key}",
    invalidCoordinates: "Invalid coordinates in %{key}",
    invalidGeometries: "Geometries should not be given in %{key} when type is not GeometryCollection",
    noOverlap: "Area %{key} must be at least a little within the master area specified",
    tooClose: "Area is too close to the specified area",
    minLength: "Line length must be over %{key}",
    areaMinHectares: "Area mush be over %{key}",
    ykjOverlap: "Geometries must be inside a single YKJ grid"
  }
};

function checkCoordinates(coordinates, type) {
  switch(type) {
  case "Point":
    return coordinates[0] && coordinates[1]
        && typeof coordinates[0] === "number"
        && typeof coordinates[1] === "number"
        && (!coordinates[2] || typeof coordinates[1] === "number")
        && coordinates[0] >= -180 && coordinates[0] <= 180
        && coordinates[1] >= -90 && coordinates[1] <= 90
        && coordinates.length <= 3;
  case "LineString":
    return coordinates[0] && coordinates[1]
      && checkCoordinates(coordinates[0], "Point")
      && checkCoordinates(coordinates[1], "Point");
  case "Polygon":
    for (const coordinateSet of coordinates) {
      for (const coordinate of coordinateSet) {
        if (!checkCoordinates(coordinate, "Point")) {
          return false;
        }
      }
    }
    return true;
  case "MultiPoint":
    for (const coordinate of coordinates) {
      if (!checkCoordinates(coordinate, "Point")) {
        return false;
      }
    }
    return true;
  case "MultiLineString":
    for (const coordinate of coordinates) {
      if (!checkCoordinates(coordinate, "LineString")) {
        return false;
      }
    }
    return true;
  default:
    return false;
  }
}

function getMessage(errorKey, options, key) {
  let msg = "%{key} is invalid geometry";
  if (options && options.message) {
    msg = typeof options.message === "string" ? options.message : (options.message[errorKey] || msg);
  }
  while (msg.includes("%{key}")) {
    msg = msg.replace("%{key}", key);
  }
  return msg;
}

function geomToPolygon(geom, value) {
  return (geom.type === "Point" && "radius" in value) ? circleToPolygon(geom.coordinates, value.radius, 8) : geom;
}

function validateBoundingBox(value, options, key) {
  const boundingBox = geojsonBounds.envelope(value).geometry;
  const squareMeters = geojsonArea.geometry(boundingBox);
  const hectares = squareMeters / 10000;
  if (hectares > options.boundingBoxMaxHectares) {
    let msg = getMessage("invalidBoundingBoxHectares", options, key);

    if (msg) {
      while(msg.includes("%{max}")) {
        msg = msg.replace("%{max}", options.boundingBoxMaxHectares);
      }
      return msg;
    }
  }
}

function validateOverlap(value, options, key) {
  const boundingBox = geojsonBounds.envelope(value);
  const targetArea = options.overlapWith;
  if ( !boundingBox
    || !boundingBox.geometry
    || !boundingBox.geometry.coordinates
    || !boundingBox.geometry.coordinates[0]
    || !boundingBox.geometry.coordinates[0][0]
    || boundingBox.geometry.coordinates[0][0].some(c => isNaN(c))) {
    return;
  }
  if (
    geojsonBounds.xMin(boundingBox) <= geojsonBounds.xMax(targetArea) &&
    geojsonBounds.xMax(boundingBox) >= geojsonBounds.xMin(targetArea) &&
    geojsonBounds.yMin(boundingBox) <= geojsonBounds.yMax(targetArea) &&
    geojsonBounds.yMax(boundingBox) >= geojsonBounds.yMin(targetArea)
  ) {
    return;
  }
  return getMessage("noOverlap", options, key);
}

function validateMinDistance(value, options, key) {
  const centroid1 = getCentroid(value);
  const centroid2 = getCentroid(normalizeGeoJson(options.minDistanceWith));

  const distance = latLngTuplesDistance([centroid1[1], centroid1[0]], [centroid2[1], centroid2[0]]);
  if (distance < options.minDistance) {
    return getMessage("tooClose", options, key);
  }
}

// TODO Validates only LineString type. Doesn't take into account MultiLineString or GeometryCollection
// containing lines, or unitGathering geometries.
function validateMinLength(value, options, key) {
  if (value.type === "LineString") {
    const dist = latLngTuplesDistance(...value.coordinates.map(([lng, lat]) => [lat, lng]));
    if (dist < options.minLineLength) {
      let msg = getMessage("minLineLength", options, key);
      while(msg.includes("%{minLength}")) {
        msg = msg.replace("%{minLength}", options.minLineLength);
      }
      while(msg.includes("%{length}")) {
        msg = msg.replace("%{length}", dist.toFixed(2));
      }
      return msg;
    }
  }
}

function validateArea(value, options, key) {
  const hectares = areaInHectares(value);
  if (hectares < options.areaMinHectares) {
    let msg = getMessage("areaMinHectares", options, key);

    if (msg) {
      while(msg.includes("%{min}")) {
        msg = msg.replace("%{min}", options.areaMinHectares);
      }
      return msg;
    }
  }
}

function _geometry(value, options, key, attributes) {

  // allow empty if no shape is required
  if (!options.requireShape && typeof value === "undefined") {
    return;
  }

  // check that the value is an object
  if (value === null || typeof value !== "object" || Array.isArray(value)) {
    return getMessage("notGeometry", options, key);
  }

  const objectKeys = Object.keys(value);
  if (objectKeys.length === 0 && !options.requireShape) {
    return;
  }

  // check that type exits and that it"s a string
  if (typeof value.type !== "string" || options.allowedTypes.indexOf(value.type) === -1) {
    return getMessage("missingType", options, key);
  }

  // check that type exits and that it"s a string
  // check that object has only allowed keys
  const allowed =  options.allowedKeys.slice(0);
  if (value.type === "Point") {
    allowed.push("radius");
    if (value.radius && typeof value.radius !== "number") {
      return getMessage("invalidRadius", options, key);
    }
  }
  if (objectKeys.filter(key => allowed.indexOf(key) === -1).length > 0) {
    return getMessage("invalidProperty", options, key);
  }

  // validate geometry collections little differently
  if (value.type === "GeometryCollection") {
    if (!value.geometries || (options.requireShape && value.geometries.length === 0)) {
      return getMessage("missingGeometries", options, key);
    }
    for (const geom of value.geometries) {
      const result = _geometry(geom, options, key, attributes);
      if (result) {
        return result;
      }
    }
    if (options.hasOwnProperty("boundingBoxMaxHectares")) {
      const geometriesWithPolygonCircles = value.geometries.map((geom) => geomToPolygon(geom, value));

      if (options.includeGatheringUnits && attributes.units) {
        attributes.units.map(unit => {
          if (unit.unitGathering && unit.unitGathering.geometry) {
            geometriesWithPolygonCircles.push(geomToPolygon(unit.unitGathering.geometry, value));
          }
        });
      }

      const result = validateBoundingBox({type: "GeometryCollection", geometries: geometriesWithPolygonCircles}, options, key);
      if (result) {
        return result;
      }
    }
    return;
  }

  // validate coordinates on simple types
  if (!value.coordinates || !checkCoordinates(value.coordinates, value.type)) {
    return getMessage("invalidCoordinates", options, key);
  }

  // check that no geometries property when not of type GeometryCollection
  if (value.geometries) {
    return getMessage("invalidGeometries", options, key);
  }
}

const getGeometriesFlat = (g) => {
  switch(g.type) {
  case "GeometryCollection":
    return g.geometries.reduce((gs, g) => [...gs, ...getGeometriesFlat(g)], []);
  default:
    return [g];
  }
};

const getCoordinatesFlat = (g) => {
  // Lat lng pair
  if (Array.isArray(g) && typeof g[0] === "number") {
    return [g];
  }
  // Array of coordinates
  if (Array.isArray(g)) {
    return g.reduce((cs, c) =>
      [...cs, ...getCoordinatesFlat(c)]
     ,[]);
  }
  switch (g.type) {
  case "FeatureCollection":
    return g.features.reduce((cs, g) => [...cs, ...getCoordinatesFlat(g.feature)], []);
  case "Feature":
    return getCoordinatesFlat(g.geometry);
  case "GeometryCollection":
    return g.geometries.reduce((cs, g) => [...cs, ...getCoordinatesFlat(g)], []);
  case "MultiPolygon":
  case "Polygon":
  case "MultiLineString":
  case "LineString":
  case "MultiPoint":
  case "Point":
    return getCoordinatesFlat(g.coordinates);
  }
};

const getGridVerbatim = (g) => {
  if (g.coordinateVerbatim && g.coordinateVerbatim.match(/^\d{3}:\d{3}$/)) {
    return [g.coordinateVerbatim, g.coordinateVerbatim].join("-");
  }
  const getGridFromYKJ = numb => ("" + (Math.floor(numb / 10000) * 10000)).substr(0,3);

  const coordinates = getCoordinatesFlat(convertGeoJSON(g, "WGS84", "EPSG:2393"));

  let west, south, east, north;

  coordinates.forEach(c => {
    const [lng, lat] = c;
    if (west === undefined || lng < west) {
      west = lng;
    }
    if (east === undefined || lng > east) {
      east = lng;
    }
    if (south === undefined || lat < south) {
      south = lat;
    }
    if (north === undefined || lat > north) {
      north = lat;
    }
  });


  return [[south, west], [north, east]].map(i => i.map(getGridFromYKJ).join(":")).join("-");
};

function validateYKJOverlap(value) {
  const geometries = getGeometriesFlat(value);
  let prevGridVerbatim;
  for (const i in geometries) {
    const g = geometries[i];
    const gridVerbatim = getGridVerbatim(g);
    const gridExtents = gridVerbatim.split("-");
    if (prevGridVerbatim && prevGridVerbatim !== gridVerbatim || gridExtents[0] !== gridExtents[1]) {
      return false;
    }
    prevGridVerbatim = gridVerbatim;
  }
  return true;
}

function normalizeGeoJson(value) {
  if (Array.isArray(value)) {
    return value.map((item) => normalizeGeoJson(item));
  }
  if (typeof value !== "object" || value === null) {
    return value;
  }
  if (value.type) {
    if (value.type === "GeometryCollection") {
      value.geometries = normalizeGeoJson(value.geometries);
      return value;
    } else if (value.type === "Point" && typeof value.radius === "number") {
      return circleToPolygon(value.coordinates, value.radius, 8);
    }
  }
  return value;
}

module.exports = function geometry(value, options, key, attributes) {
  options = util.extend({}, defaultOptions, options);
  const normalizedGeoJson = typeof value  === "object" ? normalizeGeoJson(JSON.parse(JSON.stringify(value))) : value;
  const result = _geometry(normalizedGeoJson, options, key, attributes);
  if (result) {
    return result;
  }
  if (options.hasOwnProperty("boundingBoxMaxHectares")) {
    const result = validateBoundingBox(normalizedGeoJson, options, key);
    if (result) {
      return result;
    }
  }

  if (options.overlapWith) {
    const result = validateOverlap(normalizedGeoJson, options, key);
    if (result) {
      return result;
    }
  }

  if (options.minDistanceWith) {
    const result = validateMinDistance(normalizedGeoJson, options, key);
    if (result) {
      return result;
    }
  }

  if (options.hasOwnProperty("minLineLength")) {
    const result = validateMinLength(normalizedGeoJson, options, key);
    if (result) {
      return result;
    }
  }

  if (options.hasOwnProperty("areaMinHectares")) {
    const result = validateArea(normalizedGeoJson, options, key);
    if (result) {
      return result;
    }
  }

  if (options.ykjOverlap) {
    const valid = validateYKJOverlap(value);
    if (!valid) {
      return getMessage("ykjOverlap", options, key);
    }
  }
};

/** Taken from https://github.com/arg20/circle-to-radius
 *  (Copied here because the library didn't act nice with exporting)
**/
function circleToPolygon(center, radius, numberOfSegments) {
  function toRadians(angleInDegrees) {
    return angleInDegrees * Math.PI / 180;
  }

  function toDegrees(angleInRadians) {
    return angleInRadians * 180 / Math.PI;
  }

  function offset(c1, distance, bearing) {
    var lat1 = toRadians(c1[1]);
    var lon1 = toRadians(c1[0]);
    var dByR = distance / 6378137; // distance divided by 6378137 (radius of the earth) wgs84
    var lat = Math.asin(
      Math.sin(lat1) * Math.cos(dByR) +
      Math.cos(lat1) * Math.sin(dByR) * Math.cos(bearing));
    var lon = lon1 + Math.atan2(
      Math.sin(bearing) * Math.sin(dByR) * Math.cos(lat1),
      Math.cos(dByR) - Math.sin(lat1) * Math.sin(lat));
    return [toDegrees(lon), toDegrees(lat)];
  }

  var n = numberOfSegments ? numberOfSegments : 32;
  var flatCoordinates = [];
  var coordinates = [];
  for (let i = 0; i < n; ++i) {
    flatCoordinates.push.apply(flatCoordinates, offset(center, radius, 2 * Math.PI * i / n));
  }
  flatCoordinates.push(flatCoordinates[0], flatCoordinates[1]);

  for (let i = 0, j = 0; j < flatCoordinates.length; j += 2) {
    coordinates[i++] = flatCoordinates.slice(j, j + 2);
  }

  return {
    type: "Polygon",
    coordinates: [coordinates]
  };
}

function getCentroid(value) {
  const centroid = geojsonBounds.centroid(value);

  // centroid returns [NaN, NaN] for points
  if (isNaN(centroid[0])) {
    return [geojsonBounds.xMin(value), geojsonBounds.yMin(value)];
  }

  return centroid;
}

function areaInHectares(value) {
  if (!value) {
    return;
  }
  const geometries = value.geometries ? value.geometries : [value];
  const sumArea = geometries
    .filter(({type, radius}) => (type === "Polygon" || (type === "Point" && radius)))
    .reduce((area, geometry) =>
      area + geojsonArea.geometry(geometry)
    , 0);
  return sumArea !== 0 ? sumArea / 10000 : undefined;
}
