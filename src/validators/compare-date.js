const moment = require("moment");
const util = require("../util");

const TIME_FORMATS = [moment.ISO_8601, "YYYY-MM-DD"];

const defaultOptions = {
  isAfter: undefined,
  isBefore: undefined,
  hasTimeIfOtherHas: undefined,
  message: {
    tooEarly: "%{key} must be later than %{value}.",
    tooLate: "%{key} must be earlier than %{value}.",
    hasTimeIfOtherHas: "must have time if ${target} has."
  }
};

module.exports = function compareDate(value, options, key, attributes) {
  if (!value) {
    return;
  }
  options = util.extend({}, defaultOptions, options);
  const isAfter = attributes[options.isAfter] || "";
  const isBefore = attributes[options.isBefore] || "";
  const hasTimeIfOtherHas = attributes[options.hasTimeIfOtherHas] || "";
  if (isAfter && !moment(value, TIME_FORMATS).isSameOrAfter(moment(isAfter, TIME_FORMATS))) {
    return util.errorMessage("tooEarly", options, value, isAfter);
  }
  if (isBefore && !moment(value, TIME_FORMATS).isSameOrBefore(moment(isBefore, TIME_FORMATS))) {
    return util.errorMessage("tooLate", options, value, isBefore);
  }
  if (hasTimeIfOtherHas && value && !value.includes("T") && hasTimeIfOtherHas.includes("T")) {
    let message = util.errorMessage("hasTimeIfOtherHas", options, value, hasTimeIfOtherHas);
    message = message.replace("${target}", options.hasTimeIfOtherHas);
    return message;
  }
};
