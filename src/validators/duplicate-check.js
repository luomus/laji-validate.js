const util = require("../util");

const defaultOptions = {
  key: "",
  secondaryKey: "",
  arrayPath: "",

  message: {
    isDuplicate: "%{value} is duplicate"
  }
};

module.exports = function duplicateCheck(value, options, key, attributes, globalOptions, formData) {
  options = util.extend({}, defaultOptions, options);
  let searchValue = util.parseJSONPointer(options.key, value, formData);
  if (searchValue === undefined) searchValue = util.parseJSONPointer(options.secondaryKey, value, formData);
  if (searchValue === undefined) {
    return;
  }

  let array = util.parseJSONPointer(options.arrayPath, attributes, formData);
  if (!Array.isArray(array)) {
    array = util.parseJSONPointer(options.arrayPath, value, formData);
  }
  if (!Array.isArray(array)) {
    return;
  }

  let found = 0;
  for (let i = 0; i < array.length; i++) {
    let val = util.parseJSONPointer(options.key, array[i], formData);
    if (val === undefined) val = util.parseJSONPointer(options.secondaryKey, array[i], formData);

    if (val !== undefined && val === searchValue) {
      found++;
      if (found > 1) {
        return util.errorMessage("isDuplicate", options, key, options.messageKey ? util.parseJSONPointer(options.messageKey, array[i], formData) : searchValue);
      }
    }
  }
};
