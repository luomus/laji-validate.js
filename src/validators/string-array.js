const util = require("../util");

const defaultOptions = {
  mustNotBeEmpty: false,
  message: {
    isEmpty: "%{key} can't be empty"
  }
};

module.exports = function stringArray(value, options, key) {
  options = util.extend({}, defaultOptions, options);

  let isEmpty = true;

  if (value && value.length >= 0) {
    for (let i = 0; i < value.length; i++) {
      if (value[i] && value[i] !== "") return;
    }
  }

  if (isEmpty && options.mustNotBeEmpty) {
    return util.errorMessage("isEmpty", options, key, value);
  }
};