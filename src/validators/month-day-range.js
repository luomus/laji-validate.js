const util = require("../util");
const moment = require("moment");

const defaultOptions = {
  ranges: undefined,
  dateOnly: false,
  onlyWhenField: "",
  onlyWhenFieldIn: false,
  geometryField: "geometry",
  onlyWhenYkjNorthCoordinateSmallerThan: false,
  onlyWhenYkjNorthCoordinateBiggerThan: false,
  message: {
    notInAnyRange: "is not between any of the ranges %{value}"
  }
};

function getDate(options, fields) {
  return options[fields[0]] + "." + options[fields[1]] + ".";
}

function getParsedData(value) {
  const date = moment.utc(value);
  return [+date.format("MM"), +date.format("DD")];
}

function rangesToString(ranges) {
  ranges = ranges.map((range) => {
    return getDate(range, ["earliestDay", "earliestMonth"]) + "-" + getDate(range, ["latestDay", "latestMonth"]);
  });

  return ranges.join(", ");
}

module.exports = function monthDayRange(value, options, key, attributes, globalOptions, formData) {
  options = util.extend({}, defaultOptions, options);

  // cannot do anything with value that is not string
  if (typeof value !== "string") {
    return;
  }

  if (!util.shouldValidateWithCoordinates(options, attributes, formData)) {
    return;
  }

  if (options.onlyWhenField) {
    let searchValue = util.parseJSONPointer(options.onlyWhenField, attributes, formData);
    if (searchValue === undefined) {
      return;
    }

    if (options.onlyWhenFieldIn && options.onlyWhenFieldIn.every(v => searchValue !== v)) {
      return;
    }
  }

  const data = getParsedData(value);

  if (options.ranges && options.ranges.length > 0) {
    let betweenRange = false;
    for (let i = 0; i < options.ranges.length; i++) {
      const inRange = checkIfInRange(data, options.ranges[i]);
      if (inRange) {
        betweenRange = true;
        break;
      }
    }
    if (!betweenRange) {
      return util.errorMessage("notInAnyRange", options, key, rangesToString(options.ranges));
    }
  }
};

function checkIfInRange(data, options) {
  if (!options.earliestMonth || !options.earliestDay || !options.latestMonth || !options.latestDay) {
    return true;
  }

  const reverse = options.earliestMonth > options.latestMonth;

  if (!reverse && (data[0] < options.earliestMonth || data[0] > options.latestMonth)) {
    return false;
  }

  if (reverse && (data[0] < options.earliestMonth && data[0] > options.latestMonth)) {
    return false;
  }

  if (data[0] === options.earliestMonth && data[1] < options.earliestDay) {
    return false;
  }

  if (data[0] === options.latestMonth && data[1] > options.latestDay) {
    return false;
  }

  return true;
}
