const util = require("../util");

const defaultOptions = {
  checkFields: [],

  message: {
    isDuplicate: "%{value} is duplicate"
  }
};

module.exports = function arrayDuplicateCheck(value, options, key, attributes, globalOptions, formData) {
  options = util.extend({}, defaultOptions, options);

  if (!Array.isArray(value)) {
    return;
  }

  const uniqueValues = [];
  for (let val of value) {
    if (uniqueValues.includes(val)) {
      return util.errorMessage("isDuplicate", options, key, val);
    } else {
      uniqueValues.push(val);
    }
  }

  for (let key of options.checkFields) {
    const array = util.parseJSONPointer(key, attributes, formData);
    if (Array.isArray(array)) {
      for (let val of array) {
        if (uniqueValues.includes(val)) {
          return util.errorMessage("isDuplicate", options, key, val);
        }
      }
    }
  }
};
