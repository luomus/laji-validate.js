const util = require("../util");

const defaultOptions = {
  message: {
    invalidValue: "Short hand text is invalid"
  }
};

module.exports = function waterBirdShortHand(value, options) {
  options = util.extend({}, defaultOptions, options);

  if (typeof value !== "string") {
    return;
  }

  const matches = /^[\dknpKNPÄä\,\. ]*$/.exec(value);
  if (!matches) {
    return util.errorMessage("invalidValue", options, value);
  }
};
