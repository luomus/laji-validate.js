const util = require("../util");
const validate = require("../main");

const defaultOptions = {
  check: "",
  onlyWhenCheckIn: false,
  onlyWhenCheckNotIn: false,
  onlyWhenNoCheck: false,
  requireCheck: false,
  checkNull: false,
  checkOptions: false, // for giving multiple checks
  mustBeEmpty: false,
  mustHaveValue: true,
  valueMustBeIn: false,
  geometryField: "geometry",
  onlyWhenYkjNorthCoordinateSmallerThan: false,
  onlyWhenYkjNorthCoordinateBiggerThan: false,
  message: {
    missing: "%{key} is required when %{target} is given",
    checkMissing: "%{target} is required",
    notEmpty: "%{key} has to be empty when %{target} is given",
    notValid: "%{key} is not accepted when %{target} is given%"
  }
};

function getMessage(errorKey, options, key, target) {
  let msg = "%{key} is required when %{target} is given";
  if (options && options.message) {
    msg = typeof options.message === "string" ? options.message : (options.message[errorKey] || msg);
  }
  return msg.replace("%{key}", key).replace("%{target}", target);
}

module.exports = function crossCheck(value, options, key, attributes, globalOptions, formData) {
  options = util.extend({}, defaultOptions, options);

  // check that the value is an object
  if (typeof attributes !== "object") {
    return;
  }

  if (!util.shouldValidateWithCoordinates(options, attributes, formData)) {
    return;
  }

  const checkOptions = options.checkOptions ? options.checkOptions : [options];

  for (const option of checkOptions) {
    // pick the check value
    const checkValues = util.getCheckValues(option.check, attributes, formData, option.checkNull);

    // validate check value
    if (
      (checkValues.length < 1 && !option.onlyWhenNoCheck && !option.onlyWhenCheckNotIn && !option.onlyWhenCheckEquals)
      || (checkValues.length >= 1 && option.onlyWhenNoCheck)
    ) {
      if (option.requireCheck) {
        return getMessage("checkMissing", options, key, option.check);
      }
      return;
    }

    // only if check has acceptable value
    if (option.onlyWhenCheckIn && option.onlyWhenCheckIn.every(v => checkValues.indexOf(v) === -1)) {
      return;
    }
    if (option.onlyWhenCheckNotIn && option.onlyWhenCheckNotIn.some(v => checkValues.indexOf(v) !== -1)) {
      return;
    }
    if (option.onlyWhenCheckEquals) {
      const checkAgainstValues = util.getCheckValues(option.onlyWhenCheckEquals, attributes, formData, option.checkNull);
      if (checkValues.some(v => checkAgainstValues.indexOf(v) === -1)) {
        return;
      }
    }
  }

  if (options.validators) {
    const innerValidation = validate.processValidationResults(validate.runValidations(attributes, {[key]: options.validators}, globalOptions || {}, formData), globalOptions || {});
    if (innerValidation && innerValidation[key] && innerValidation[key].length) {
      return Array.isArray(innerValidation[key]) && innerValidation[key].length === 1
        ? innerValidation[key][0]
        : innerValidation[key];
    }
    return;
  }

  // Check that the given
  let errorKey = "missing";
  if (((typeof value === "string" || Array.isArray(value)) && value.length > 0) ||
    (typeof value === "number" && !isNaN(value)) || (typeof value === "boolean")) {
    if (options.mustBeEmpty) {
      errorKey = "notEmpty";
    } else if (options.valueMustBeIn && options.valueMustBeIn.indexOf(value) === -1) {
      errorKey = "notValid";
    } else {
      return;
    }
  } else if (options.mustBeEmpty || !options.mustHaveValue) {
    return;
  }

  return getMessage(errorKey, options, key, options.check);
};
