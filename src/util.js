const geojsonBounds = require("geojson-bounds");
const { convertLatLng } = require("@luomus/laji-map/lib/utils");

function _findRoot(path, object, fullObject) {
  if (path[0] === "#") {
    path.shift();
    return fullObject;
  } else if (path[0] !== "..") {
    return object;
  }

  const childKeys = {
    gatheringEvent: {},
    gatherings: {
      units: {
        unitFact: {},
        identifications: {},
        measurement: {}
      },
      gatheringFact: {},
      measurement: {}
    }
  };

  // First try to get it the fast way
  let parents = _findParents(object, fullObject, [], childKeys);
  if (!parents) {
    // if parent where not found use the brute force way
    parents = _findParents(object, fullObject, []);
  }

  if (!parents) {
    // No parents where found so returning empty handed
    return {};
  }

  let root = {};
  while(path[0] === "..") {
    root = parents.pop();
    path.shift();
  }
  return root;
}

function _findParents(object, fullObject, parents, travelBy) {
  if (object === fullObject) {
    return parents;
  }
  if (typeof travelBy === "undefined") {
    travelBy = fullObject;
  }
  if (typeof travelBy !== "object" || travelBy === null) {
    return false;
  }

  let result = false;
  const properties = Object.keys(travelBy);
  const newParents = parents.slice();

  newParents.push(fullObject);
  for (let i of properties) {
    if (fullObject[i]) {
      if (Array.isArray(fullObject[i])) {
        for (let idx in fullObject[i]) {
          if (!fullObject[i].hasOwnProperty(idx)) {
            continue;
          }
          if (object === fullObject[i]) {
            return newParents;
          }
          result = _findParents(object, fullObject[i][idx], newParents, Array.isArray(travelBy[i]) ? travelBy[i][idx] : travelBy[i]);
          if (result) {
            return result;
          }
        }
      } else if (typeof fullObject[i] === "object" && fullObject[i] !== null) {
        result = _findParents(object, fullObject[i], newParents, travelBy[i]);
        if (result) {
          return result;
        }
      }
    }
  }
  return result;
}

module.exports = {
  extend: function(obj) {
    [].slice.call(arguments, 1).forEach(function(source) {
      for (var attr in source) {
        obj[attr] = source[attr];
      }
    });
    return obj;
  },
  errorMessage: function getMessage(errorKey, options, key, value) {
    let msg = "%{key} is invalid";
    if (options && options.message) {
      msg = typeof options.message === "string" ? options.message : (options.message[errorKey] || msg);
    }
    return msg.replace("%{key}", key).replace("%{value}", value);
  },
  parseJSONPointer: function (path, object, fullObject) {
    const splitted = path.split("/").filter(s => s.length);
    object = _findRoot(splitted, object, fullObject);
    return splitted.reduce((o, s, i) => {
      [["~1", "/"], ["~0", "~"]].forEach(decoderPair => {
        const [escaped, decoded] = decoderPair;
        while (s.includes(escaped)) {
          s = s.replace(escaped, decoded);
        }
      });
      return (s in o || i === splitted.length - 1) ? o[s] : {};
    }, object || {});
  },
  isJSONPointer: function(pointer) {
    return typeof pointer === "string" && (pointer[0] === "/" || pointer[0] === "#");
  },
  getCheckValues(check, attributes, formData, checkNull) {
    if (Array.isArray(check)) {
      return check.reduce((arr, curr) => {
        arr.push.apply(arr, this.getCheckValues(curr, attributes, formData, checkNull));
        return arr;
      }, []);
    }

    const splitted = check.split(/\*(.+)/);
    if (splitted.length === 1) {
      let value = this.parseJSONPointer(check, attributes, formData);
      if (value === undefined) {
        value = null;
      }
      return (checkNull || value != null)
        ? (Array.isArray(value) ? value : [value])
        : [];
    } else {
      const array = this.parseJSONPointer(splitted[0], attributes, formData);
      if (!Array.isArray(array)) { return []; }
      return array.reduce((arr, curr) => {
        arr.push.apply(arr, this.getCheckValues(splitted[1], curr, formData, checkNull));
        return arr;
      }, []);
    }
  },
  shouldValidateWithCoordinates(options, attributes, formData) {
    if (options.onlyWhenYkjNorthCoordinateSmallerThan || options.onlyWhenYkjNorthCoordinateBiggerThan) {
      const geometryValue = this.parseJSONPointer(options.geometryField, attributes, formData);
      if (!geometryValue) {
        return false;
      }

      const maxNorthCoordinate = geojsonBounds.yMax(geometryValue);

      if (options.onlyWhenYkjNorthCoordinateSmallerThan) {
        const threshold = convertLatLng([
          options.onlyWhenYkjNorthCoordinateSmallerThan * 10000, 3333333
        ], "EPSG:2393", "WGS84")[0];

        if (maxNorthCoordinate >= threshold) {
          return false;
        }
      }
      if (options.onlyWhenYkjNorthCoordinateBiggerThan) {
        const threshold = convertLatLng([
          options.onlyWhenYkjNorthCoordinateBiggerThan * 10000, 3333333
        ], "EPSG:2393", "WGS84")[0];

        if (maxNorthCoordinate <= threshold) {
          return false;
        }
      }
    }
    return true;
  }
};
